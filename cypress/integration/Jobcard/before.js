const commonLocators = require("../../Locators/commonLocators.json")
const dashboardLocators = require("../../Locators/dashboard.json")
before(() => {
    // Login in to app.
    // cy.loginWithUI()

    // Login using Api
    // cy.loginWithApi(Cypress.env("Username"), Cypress.env("Password"))
    cy.fixture("example").then(data => {
        cy.loginWithApi(data.credentials["Username"], data.credentials["Password"])
    })

    // cy.fixture("userData.json").then(data => {
    //     window.localStorage.setItem("userData", JSON.stringify(data));
    // })
    
    cy.visit("/jobcard")
    // Click Get Started button to open form
    // cy.get(dashboardLocators.dashboardHeading).should("have.text", "Dashboard")
})