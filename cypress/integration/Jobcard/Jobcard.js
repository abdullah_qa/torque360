/// <reference types="Cypress" />

import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"
import 'cypress-file-upload'

const commonLocators = require("../../Locators/commonLocators.json")
const InspectionLocators = require("../../Locators/Inspection.json")
const EstimateLocators = require("../../Locators/EstimateLocators.json")
const JobcardLocators = require("../../Locators/JobcardLocators.json")

Given("the user is on New Jobcard screen.", () => {
    // cy.get(commonLocators.navNewActivityBtn).click()
    // cy.get(commonLocators.navOptions).contains("Jobcard").click({ force: true })
    cy.get("h4").should("contain", "New Jobcard")
})

Then("the {string} field should be visible.", (field) => {
    cy.get(JobcardLocators[field]).should("be.visible")
})

Then("the {string} button from {string} should not be Clickable.", (btn, buttonGroup) => {
    cy.get(JobcardLocators[buttonGroup]).contains(btn).parent().should("be.disabled")
})

Then("the {string} field should not be Clickable.", (btn) => {
    cy.get(JobcardLocators[btn]).should("be.disabled")
})

Then("the {string} field with text {string} should be visible.", (field, fieldText) => {
    cy.get(JobcardLocators[field]).should("have.text", fieldText)
})

When("the user hits the {string} button.", (field) => {
    cy.get(JobcardLocators[field]).click()
})

When("the user hits the {string} button from the {string}.", (btn, buttonGroup) => {
    cy.get(JobcardLocators[buttonGroup]).contains(btn).click()
})

When("the user selects {string} meter.", (field) => {
    cy.fixture('jobcard_examples').then(data => {
        cy.get(JobcardLocators[field]).eq(data[field]).click()

    })
})
When("the user upload {string} image for {string}.", (image, imageHolder) => {
    cy.get(JobcardLocators[imageHolder]).attachFile(image)
    // cy.get(JobcardLocators[imageHolder]).next().children().first().should("have.a.property", "style")
})

Then("the {string} error appears under profile image.", (errorMessage) => {
    cy.get(JobcardLocators.errorMessage).should("have.text", errorMessage)
})

When("the user enters valid value in {string}.", (field) => {
    cy.fixture('jobcard_examples').then(data => {
        cy.get(JobcardLocators[field]).clear()
        cy.get(JobcardLocators[field]).type(data[field])
    })
})

When("the user enters invalid value in {string}.", (field) => {
    cy.fixture('jobcard_examples').then(data => {
        cy.get(JobcardLocators[field]).clear()
        cy.get(JobcardLocators[field]).type(data[field])
    })
})

When("the user enters space in {string}.", (field) => {
    cy.get(JobcardLocators[field]).type("  ")
})

When("the user enters no value in {string}.", (field) => {
    cy.get(JobcardLocators[field]).clear()
})

When("the user selects {string} value from the {string} dropdown.", (option, dropdown) => {
    cy.fixture('jobcard_examples').then(data => {
        cy.get(JobcardLocators[dropdown]).click()
        cy.get(commonLocators.dropdownOptions).contains(option).click()
    })
})

When("the user selects no value from the {string}.", (dropdown) => {
    // cy.fixture('onboarding_examples').then(data => {
    //     cy.get(onboardingLocators[dropdown]).clear()
    // })
    // pass
})

When("the user selects {string}.", (field) => {
    cy.fixture('jobcard_examples').then(data => {
        for (var job in data[field]){
            cy.get(JobcardLocators[field]).contains(data[field][job]).click()
        }
    })
})

Then("the {string} field should appear.", (field) => {
    cy.get(JobcardLocators[field]).should("be.visible")
})

Then("the {string} error should appear.", (errorMessage) => {
    cy.get(JobcardLocators.inspectionTypeError).should("have.text", errorMessage)
})

Then("the {string} success message should appear.", (message) => {
    cy.get(JobcardLocators.successTitle).should("have.text", message)
    // cy.fixture('jobcard_examples').then(data => {
    //     cy.getUniqueName(data.inspectionTitle).then(newInspectionTitle => {
    //         data.inspectionTitle = newInspectionTitle
    //         cy.writeFile("cypress/fixtures/jobcard_examples.json", JSON.stringify(data))
    //     })    
    // })
            
})

When("the user selects customer and Vehicle, the data appear should be correct.", async () => {
    cy.fixture("customer_examples").then(customerVehicleData => {
        let data = customerVehicleData.validData
        cy.get(JobcardLocators.searchCustomer).clear()
        cy.get(JobcardLocators.searchCustomer).type(data.Customer.phone)

        cy.get(JobcardLocators.dropdownOptionButtons).should("be.visible")
        // Storing the customer data
        cy.get(JobcardLocators.customerData).contains(data.Customer.phone).parents(JobcardLocators.customerData).invoke("text").then(fieldText => {
            let expectedCustomerData = fieldText.replace(/ /g, '')
            cy.log(expectedCustomerData)

            // Selecting customer
            cy.get(JobcardLocators.dropdownOptionButtons).contains(data.Customer.phone).click()

            // Storing the Vehicle data
            cy.get(JobcardLocators.vehicleData).contains(data.Vehicle.vehiclePlate).parents(JobcardLocators.vehicleData).invoke("text").then(fieldText => {
                let expectedVehicleData = fieldText.replace(/ /g, '') // qa
                cy.log(expectedVehicleData)

                // Selecting vehicle
                cy.get(JobcardLocators.vehicleData).contains(data.Vehicle.vehiclePlate).click()

                // Asserting the Customer data
                cy.get(JobcardLocators.selectedcustomerData).first().invoke("text").then(fieldText => {
                    let observedCustomerData = fieldText.replace(/ /g, '') // qa
                    cy.log(observedCustomerData)
                    expect(expectedCustomerData).to.be.equal(observedCustomerData)

                    // Asserting the Vehicle data
                    cy.get(JobcardLocators.selectedvehicleData).last().invoke("text").then(fieldText => {
                        let observedVehicleData = fieldText.replace(/ /g, '') // qa
                        cy.log(observedVehicleData)
                        // Will Enable when the issue fixed
                        // expect(expectedVehicleData).to.be.equal(observedVehicleData)
                    })

                })

            })

        })
    })

})

When("the user store {string} data.", (cv) => {
    cy.get(JobcardLocators.dropdownOptionButtons).should("be.visible")
    cy.get(JobcardLocators[cv]).invoke("text").then(fieldText => {
        let txt = fieldText.replace(/ /g, '') // qa
        cy.log(txt)
        cy.readFile("cypress/fixtures/jobcard_examples.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then((data) => {
            data["searched" + cv] = txt
            cy.writeFile("cypress/fixtures/jobcard_examples.json", JSON.stringify(data))
        })
    })
})

Then("the customer should show correct {string} data.", (cv) => {
    let n = 0
    if (cv === "selectedvehicleData") {
        n = 1
    }

    cy.wait(15000)
    cy.get(JobcardLocators[cv]).eq(n).invoke("text").then(fieldText => {
        let txt = fieldText.replace(/ /g, '') // qa
        cy.log(txt)
        cy.fixture("onboarding_examples").then(data => {
            expect(txt).to.equal(data["letsBeginMessage"])
        })
    })

})

Then("the preview should show correct value for {string} against {string}.", (field, previewFieldName) => {
    cy.fixture("jobcard_examples").then(data => {
        let expectedValue = data[field]
        cy.get(JobcardLocators.previewField).contains(previewFieldName).parent().find(JobcardLocators.previewFieldValue).should("have.text", expectedValue)

    })
})

When("positive flow", () => {

    cy.get(commonLocators.navNewActivityBtn).click()
    cy.get(commonLocators.navOptions).contains("Inspection").click({ force: true })

    cy.get(JobcardLocators.addCustomerBtn).contains("Add Customer").should("be.visible")

    // Create customer
    // cy.get(JobcardLocators.addCustomerBtn).contains("Add Customer").click()


    cy.get(JobcardLocators.searchByDropdown).click()
    cy.get(commonLocators.dropdownOptions).contains("Customer Name").click()
    cy.get(JobcardLocators.searchCustomer).type("qa")

    cy.get(JobcardLocators.dropdownOptionButtons).contains("qa").click()
    cy.get("span").contains("Color:").click()

    cy.get(JobcardLocators.inspectionTypeBtns).contains("General Inspection Template").click()

    cy.get(JobcardLocators.radioGroup).eq(5).find(JobcardLocators.badCondition).click({ force: true })
    cy.get(JobcardLocators.radioGroup).eq(7).find(JobcardLocators.ModerateCondition).click({ force: true })

    cy.get(commonLocators.dialogBtns).contains("Save").click()

    cy.wait(2000)
    cy.get(JobcardLocators.generateBtns).contains("Generate DVI").click()
    cy.get(JobcardLocators.signatureCanvas).should("be.visible")

    cy.get(JobcardLocators.signatureCanvas).click({ multiple: true }, { force: true }).type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}")
    cy.get(JobcardLocators.authorizeBtn).click()

    cy.get(JobcardLocators.successTitle).should("have.text", "Inspection has been created")
})