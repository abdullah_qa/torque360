/// <reference types="Cypress" />

import 'cypress-file-upload'
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

Given("When user generates VIN and find record online.", () => {
    cy.visit("https://www.carmd.com/free-vin-decoder")
    let tempArray = []
    let start = 0
    let total = 10
    let vehicleData = ""
    while (start != total) {

        cy.readFile("cypress/fixtures/vins.json").then((data) => {
            cy.request({
                method: "GET",
                url: "https://randomvin.com/getvin.php",
                body: { 
                    "type": "real" 
                }
            }).then((response) => {
                expect(response.status).equal(200)
                cy.log(response.body)
                let vin = response.body.replace(/\n/g, '')

                // cy.intercept("GET", "/free-vin-decoder/?freeVinDecoder=" + vin).as("abc")

                cy.get("#freeVinDecoder").clear().type(vin)

                // cy.wait("@abc").then((interception) => {
                //     cy.log(interception.response.body)
                // })
                cy.get(".vinDecoderResult h5").should("not.have.text", vehicleData).invoke("text").then(col => {
                    cy.log(col)
                    data[vin] = col
                    vehicleData = col
                    if (start == total) {
                        cy.writeFile("cypress/fixtures/vins.json", JSON.stringify(data))
                    }
                })

                cy.wait(2000)

                // cy.request({
                //     method: "GET",
                //     url: "https://www.carmd.com/free-vin-decoder/",
                //     headers: {
                //         "X-Requested-With": "XMLHttpRequest",
                //         "Host": "www.carmd.com",
                //         "Referer": "https://www.carmd.com/free-vin-decoder/"
                //     },
                //     body: { 
                //         "freeVinDecoder": vin
                //     }
                // }).then((response) => {
                //     expect(response.status).equal(200)
                //     cy.log(response.body)
                // })
            })

            // Finally writting to file.
        })
        ++start

    }
})

Given("Using Api - When user generates {string} VINs and find record online.", (n) => {
    let start = 0
    let total = parseInt(n)
    let vehicleData = ""
    while (start != total) {

        cy.readFile("cypress/fixtures/vins.json").then((data) => {
            cy.request({
                method: "GET",
                url: "https://randomvin.com/getvin.php",
                body: { 
                    "type": "real" 
                }
            }).then((response) => {
                expect(response.status).equal(200)
                cy.log(response.body)
                let vin = response.body.replace(/\n/g, '')

                cy.request({
                    method: "POST",
                    url: "https://vinpit.com/api/search",
                    // headers: {
                    //     "X-Requested-With": "XMLHttpRequest",
                    //     "Host": "www.carmd.com",
                    //     "Referer": "https://www.carmd.com/free-vin-decoder/"
                    // },
                    body: { 
                        "vin": vin
                    }
                }).then((response) => {
                    expect(response.status).equal(200)
                    cy.log(JSON.stringify(response.body))
                })
            })

            // Finally writting to file.
        })
        ++start

    }
})
