const commonLocators = require("../../Locators/commonLocators.json")
const dashboardLocators = require("../../Locators/dashboard.json")

Then("the {string} validation error should appear.", (validationError) => {
    cy.get(commonLocators.validationError).contains(validationError).should("have.text", validationError)
})

Then("the {string} validation error should appear against phone number field.", (validationError) => {
    cy.get(commonLocators.phoneValidationError).should("have.text", validationError)
})

Given("The user is on Dashboard.", () => {
    // cy.wait(5000)
    cy.visit("/dashboard")
    // Click Get Started button to open form
    cy.get(dashboardLocators.dashboardHeading).should("have.text", "Dashboard")
})