Feature: Jobcard

    This Feature: contains all the test cases of Jobcard CRUD options

    # Scenario: positive flow
    # When positive flow
    
    Scenario: User should not be allowed to Generate Jobcard without entering any field.
    Given the user is on New Jobcard screen.
    Then the "Create Job Card" button from "generateBtns" should not be Clickable.
        # And the "otherJobType" field should not be Clickable.
        And the "Save Draft" button from "generateBtns" should not be Clickable.
        
    Scenario: User should NOT be allowed to Generate DVI by adding customer/vehicle but without general/detailed inspection.
    When the user selects "Mobile Number" value from the "searchByDropdown" dropdown.
    When the user selects customer and Vehicle, the data appear should be correct.
    # When the user hits the "generateJobcardBtn" button.
    # Then the "Select atleast one inspection type." error should appear.

    # Todo: Customer and vehicle preview validations. 

    Scenario: Create Jobcard
    When the user enters valid value in "keyLocation".
        And the user selects "jobTypes".
        # And the user enters valid value in "otherJobType".
        And the user enters valid value in "notes".
        And the user enters valid value in "keyLocation".
        And the user hits the "Create Job Card" button from the "generateBtns".
    Then the "Jobcard has been created" success message should appear.
