Feature: Sign Up

    Description: This feature test all test cases of Sign Up. 

    Background: The user is on the Sign Up screen. 
    Given The user is on the Sign Up page.
    # <space> in name 
    
    Scenario: Sign up with all blank fields
    When the user clicks Submit button
    Then the "Name Required" validation error should appear.
    Then the "Email Required" validation error should appear.
    Then the "Phone Number Required" validation error should appear.
    Then the "Password Required" validation error should appear.

    Scenario: Sign up with blank name field.
    When the user enters no value in "nameField" field.
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "Name Required" validation error should appear.
    # When the user enters Name: " "
    #     And the user clicks Submit button
    # Then the "Name Required" validation error should appear.

    Scenario: Sign up with blank email field.
    When the user enters Name: "SQAE"
        And the user enters no value in "emailField" field.
        And the user enters phone: "5452345323"
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "Email Required" validation error should appear.

    Scenario: Sign up with invalid email field.
    When the user enters Name: "SQAE"
        And the user enters "InValidEmail" in Email field
        And the user enters phone: "5452345323"
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "Invalid Email" validation error should appear.

    Scenario: Sign up with already existing email fields.
    When the user enters Name: "SQAE"
        And the user enters "AlreadyExistingEmail" in Email field
        And the user enters phone: "5452345323"
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "AlreadyExistingEmailError" should appear.

    Scenario: Sign up with blank phone field.
    When the user enters Name: "SQAE"
        And the user enters "validEmail" in Email field
        And the user enters no value in "phoneField" field.
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "Phone Number Required" validation error should appear.

    Scenario: Sign up with invalid phone field.
    When the user enters Name: "SQAE"
        And the user enters "validEmail" in Email field
        And the user enters phone: "54523453"
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "Invalid Phone Number" validation error should appear.

    Scenario: Sign up with invaild passwords.
    When the user enters Name: "SQAE"
        And the user enters no value in "emailField" field.
        And the user enters no value in "passwordField" field.
        And the user enters "validEmail" in Email field
        # And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Password Required" validation error should appear.
        And the color of the text: "At least contains 1 capital letter" should be "rgba(174, 182, 183, 0.796)"
        And the color of the text: "At least contains 1 Symbol" should be "rgba(174, 182, 183, 0.796)"
        And the color of the text: "At least contains 1 digit" should be "rgba(174, 182, 183, 0.796)"
        And the color of the text: "Minimum 8 Characters" should be "rgba(174, 182, 183, 0.796)"
    When the user enters "T" in Password field
        And the user clicks Submit button
    Then the "Password Policy:" validation error should appear.
        And the color of the text: "At least contains 1 capital letter" should be "rgb(33, 196, 108)"
    When the user enters "orq@" in Password field
        And the user clicks Submit button
    Then the "Password Policy:" validation error should appear.
        And the color of the text: "At least contains 1 Symbol" should be "rgb(33, 196, 108)"
    When the user enters "3" in Password field
        And the user clicks Submit button
    Then the "Password Policy:" validation error should appear.
        And the color of the text: "At least contains 1 digit" should be "rgb(33, 196, 108)"
    When the user enters "60" in Password field
        And the user clicks Submit button
    Then the color of the text: "Minimum 8 Characters" should be "rgb(33, 196, 108)"

    Scenario: The user should not Sign Up with exceeding character limits data. 
    When the user enters data with exceeding characters in all fields, then "Character Limit Exceeded" error should appear.

    Scenario: Sign up with all valid fields.
    When the user enters Name: "SQAE"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user enters "validPassword" in Password field w.r.t the password policies.
        And the user clicks Submit button
    Then the "SuccessMessage" should appear.

    Scenario: Get registration URL from email, naviagete there and activate the account.
    Given the user navigates to the registration URL received at user email address. 
    Then the "Thank you for activating your account" should appear.    
