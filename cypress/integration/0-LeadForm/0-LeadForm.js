/// <reference types="Cypress" />

import { And, Then, When } from "cypress-cucumber-preprocessor/steps"
const LeadForm = require("../../Locators/LeadForm.json")
const commonLocators = require("../../Locators/commonLocators.json")

Given("Go to branding site and Open Lead form", () => {
    // cy.intercept("GET", "https://www.google.com").as("site")
    // cy.visit("https://develop.torque360.co/")
    // cy.wait("@site")
    // Click Get Started button to open form
    cy.get(LeadForm.bookADemoButton).first().click()
    cy.get(LeadForm.leadFormLogo).should("be.visible")
})

Then("the user enters valid Name: {string}", (nameField) => {
    // Fill all fields
    cy.get(LeadForm.nameField).type(nameField)  
})

And("the user enters website: {string}", (websiteField) => {
    // Fill all fields
    cy.get(LeadForm.websiteField).type(websiteField)
})

And("the user enters {string} in Email field", (emailStatus) => {
    // Fill EMAIL field
    cy.fixture('example').then(data => {
        cy.get(commonLocators.emailField).type(data.leadFromEmaiData[emailStatus])
    })
})

And("the user enters phone: {string}", (phoneField) => {
    // Fill all fields
    cy.get(LeadForm.phoneField).type(phoneField)
})

And("the user clicks Submit button", () => {
    // Click Submit button
    cy.get(LeadForm.submitButton).click()
})

And("the user clicks Close button", () => {
    // Click Close button
    cy.get(LeadForm.closeButton).click()
})

Then("the form should not be visible.", () => {
    cy.get(LeadForm.leadFormLogo).should("not.be.visible")
})
Then("the {string} should appear.", (toastMessage) => {
    // Reading fixture file.
    cy.fixture('example').then(data => {
        cy.get(commonLocators.toastMessage).should("have.text", data.leadFromEmaiData[toastMessage])
    })

    if (toastMessage === "SuccessMessage"){
        cy.readFile("cypress/fixtures/example.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then((data) => {
            // get Unique Email.
            // data.leadFromEmaiData.validEmail = getUniqueEmail(data.leadFromEmaiData.validEmail)
            // cy.writeFile("cypress/fixtures/example.json", JSON.stringify(data))
            cy.getUniqueEmail(data.leadFromEmaiData.validEmail).then(newEmail => {
                data.leadFromEmaiData.validEmail = newEmail
                cy.writeFile("cypress/fixtures/example.json", JSON.stringify(data))
            })
        })
        // cy.writeFile('/cypress/fixtures/example.json', { "validEmail": "qa.abdullah360+9@gmail.com" })
    }
})
