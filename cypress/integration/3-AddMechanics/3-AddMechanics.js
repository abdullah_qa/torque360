/// <reference types="Cypress" />

import 'cypress-file-upload'
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

const commonLocators = require("../../Locators/commonLocators.json")
const addMechanicsLocators = require("../../Locators/AddMechanics.json")

Given("the user navigates to Add Mechanics screen.", () => {
    // cy.visit("/dashboard")
    cy.visit("/addmechanics")
    // Get link from email. 
})

Then("the {string} field should be visible.", (field) => {
    cy.get(addMechanicsLocators[field]).should("be.visible")
})

When("the user hits the {string} button.", (btn) => {
    cy.get(addMechanicsLocators[btn]).click()
})

When("the user enters valid value in {string}.", (field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(addMechanicsLocators[field]).clear()
        cy.get(addMechanicsLocators[field]).type(data.mechanicsData[field])
    })
})

When("the user enters invalid value in {string}.", (field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(addMechanicsLocators[field]).clear()
        cy.get(addMechanicsLocators[field]).type(data.mechanicsData.invalidValues[field])
    })
})

When("the user enters {string} value in {string}.", (AlreadyExistingEmail, field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(addMechanicsLocators[field]).clear()
        cy.get(addMechanicsLocators[field]).type(data.mechanicsData[AlreadyExistingEmail])
    })
})

When("the user enters space in {string}.", (field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(addMechanicsLocators[field]).type("  ")
    })
})

When("the user enters no value in {string}.", (field) => {
    cy.get(addMechanicsLocators[field]).clear()
})
And("the user enters {string} in Email field", (emailStatus) => {
    // Fill EMAIL field
    cy.fixture('onboarding_examples').then(data => {
        cy.get(addMechanicsLocators.emailField).clear()
        cy.get(addMechanicsLocators.emailField).type(data.mechanicsData[emailStatus])
    })
})

And("the user enters phone: {string}", (phoneField) => {
    // Fill all fields
    cy.get(addMechanicsLocators.phoneField).type(phoneField)
})

And("the user clicks Submit button", () => {
    // Click Submit button
    cy.get(addMechanicsLocators.submitButton).click()
})

And("the user clicks Close button", () => {
    // Click Close button
    cy.get(addMechanicsLocators.closeButton).click()
})

Then("the form should not be visible.", () => {
    cy.get(addMechanicsLocators.leadFormLogo).should("not.be.visible")
})
Then("the {string} should appear.", (toastMessage) => {
    // Reading fixture file.
    cy.fixture('onboarding_examples').then(data => {
        cy.get(commonLocators.toastMessage).should("have.text", data.mechanicsData[toastMessage])
    })

    if (toastMessage === "SuccessMessage"){
        cy.readFile("cypress/fixtures/onboarding_examples.json", (err, data) => {
            if (err) {
                cy.log(err)
            };
        }).then((data) => {
            // get Unique Email.
            cy.getUniqueEmpEmail(data.mechanicsData.emailField).then(newEmail => {
                data.mechanicsData.AlreadyExistingEmail = data.mechanicsData.emailField
                data.mechanicsData.emailField = newEmail
                cy.writeFile("cypress/fixtures/onboarding_examples.json", JSON.stringify(data))
            })
        })
        // cy.writeFile('/cypress/fixtures/onboarding_examples.json', { "emailField": "qa.abdullah360+9@gmail.com" })
    }
})

Given("the user navigates to the registration URL received at employee email address.", () => {
    cy.fixture('onboarding_examples').then(data => {
        cy.gotoRegLinkFromEmail(Cypress.env("MAILOSAUR_Server_ID"), data.mechanicsData.AlreadyExistingEmail)
    })
})
 
And("the user enters {string} in Password field", (password) => {
    // Invalid password field assertions
    cy.get(addMechanicsLocators.passwordField).type(password)
})

And("the color of the text: {string} should be {string}", (errorMessage, textColor) => {
    cy.get(addMechanicsLocators.passwordError).contains(errorMessage).should("have.css", "color", textColor)
})

Then("the {string} with text {string} should appear.", (field, message) => {
    // Reading fixture file.
    cy.fixture('onboarding_examples').then(data => {
        cy.get(addMechanicsLocators[field]).should("have.text", data.mechanicsData[message])
    })
})