before(() => {
    // Login in to app.
    // cy.loginWithUI()

    // Login using Api
    // cy.loginWithApi(Cypress.env("Username"), Cypress.env("Password"))
    cy.fixture("example").then(data => {
        cy.loginWithApi(data.credentials["Username"], data.credentials["Password"])
    })
})