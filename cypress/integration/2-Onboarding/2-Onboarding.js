/// <reference types="Cypress" />

import 'cypress-file-upload'
import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

const commonLocators = require("../../Locators/commonLocators.json")
const onboardingLocators = require("../../Locators/Onboarding.json")
const addMechanicsLocators = require("../../Locators/AddMechanics.json")

And("the user navigates to onboarding screen.", () => {
    // cy.visit("/dashboard")
    // cy.visit("/")
    // cy.wait(2000)
    // cy.get(dashboardLocators.dashboardHeading).should("have.text", "Dashboard")

    // cy.get(onboardingLocators.onboardingBtn).click()
    // cy.wait(2000)
    cy.get(onboardingLocators.letsBeginBtn).should("be.visible")

    cy.fixture('onboarding_examples').then(data => {
        cy.get(onboardingLocators.letsBeginMessage).should("have.text", data.letsBeginMessage)
    })

    cy.get(onboardingLocators.letsBeginBtn).click()
    cy.get(commonLocators.errorUnderLine).should("not.exist")
})

Then("the {string} field should be visible.", (field) => {
    cy.get(onboardingLocators[field]).should("be.visible")
})

When("the user hits the {string} button.", (btn) => {
    cy.get(onboardingLocators.OnboardingActionsButtons).contains(btn).click()
})

When("the user upload {string} for profile.", (image) => {
    cy.get(onboardingLocators.profileImage).attachFile(image)
    cy.get(onboardingLocators.profileImage).parent().next().should("not.have.a.property", "style")
})

Then("the {string} error appears under profile image.", (errorMessage) => {
    cy.get(onboardingLocators.errorMessage).should("have.text", errorMessage)
})

When("the user enters valid value in {string}.", (field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(onboardingLocators[field]).clear()
        cy.get(onboardingLocators[field]).type(data[field])
    })
})

When("the user enters invalid value in {string}.", (field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(onboardingLocators[field]).clear()
        cy.get(onboardingLocators[field]).type(data.invalidStepOneValues[field])
    })
})

When("the user enters space in {string}.", (field) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(onboardingLocators[field]).type("  ")
    })
})

When("the user enters no value in {string}.", (field) => {
    cy.get(onboardingLocators[field]).clear()
})

When("the user selects valid value from the {string}.", (dropdown) => {
    cy.fixture('onboarding_examples').then(data => {
        cy.get(onboardingLocators[dropdown]).click()
        cy.get(commonLocators.dropdownOptions).contains(data[dropdown]).click()
    })
})

When("the user selects no value from the {string}.", (dropdown) => {
    // cy.fixture('onboarding_examples').then(data => {
    //     cy.get(onboardingLocators[dropdown]).clear()
    // })
    // pass
})

Then("the {string} field should be underlined as red.", (field) => {
    cy.getAfterValue(onboardingLocators[field], 'after', 'border-bottom-color').should("eq", "rgb(229, 57, 53)")
})

Then("the {string} field should NOT be underlined as red.", (field) => {
    cy.getAfterValue(onboardingLocators[field], 'after', 'border-bottom-color').should("eq", "rgb(105, 33, 196)")
})

Then("the {string} label should be highlighted as Red.", (label) => {
    cy.get(onboardingLocators[label]).should("have.css", "color", "rgb(255, 0, 0)")
})

When("the user add total {string} out of {string} through the slider.", (slider, maxValue) => { 
    cy.get(onboardingLocators.inputSlider + "[aria-valuemax=" + maxValue + "]").click({force: true})
    cy.get(onboardingLocators.inputSlider + "[aria-valuemax=" + maxValue + "]").type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}")
})

Then("the all features and services should be visible.", () => {
    let tempArray = []

    cy.fixture("onboarding_examples").then(data => {
        cy.get(onboardingLocators.featureAndOperationsCheckSpan).each(($col, index, $list) => {
            let colText = $col.text()
            expect(data.features).include(colText)
            cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(index).should("have.css", "color", "rgb(200, 201, 224)")
            // tempArray[index] = colText
            // if (index >= $list.length - 1) {
            //     cy.writeFile("cypress/fixtures/features.json", JSON.stringify(tempArray))
            // }
        })
    })
    
})

When("the user chooses {string} for {string} options.", (option, group) => {
    cy.get(onboardingLocators.allRadioOptions).each(($col, index, $list) => {
        let radioText = $col.text()
        if (radioText === option){
            cy.get($col).prev().find(onboardingLocators.radioButton).click()
        }
    })
})

When("the user selects features from the available.", () => {
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(4).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(5).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(7).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(2).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(8).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(14).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(15).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(17).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(12).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(18).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(24).click()
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(26).click()

    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(4).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(5).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(7).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(2).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(8).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(14).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(15).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(17).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(12).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(18).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(24).should("have.css", "background-color", "rgb(105, 33, 196)")
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(26).should("have.css", "background-color", "rgb(105, 33, 196)")

})
Then("the {string} should appear.", (toastMessage) => {
    cy.fixture('example').then(data => {
        cy.get(commonLocators.toastMessage).should("have.text", data[toastMessage])
    })
})

When("Positive flow", () => {

    // cy.intercept("POST", "https://api.torque360.co/api/stores/add").as("onboarding")
    // cy.get(onboardingLocators.letsBeginBtn).click()
    // cy.wait(2000)
    // cy.get(onboardingLocators.profileImage).attachFile("valid_image.png")
    // cy.get(onboardingLocators.addressField).type("32 washington pl, new york.")
    // cy.get(onboardingLocators.mobileField).clear()
    // cy.get(onboardingLocators.mobileField).type("1111111111")
    // cy.get(onboardingLocators.businessNameField).type("Owner QA")
    // cy.get(commonLocators.emailField).type("qa.abdullah360+admin@gmail.com")

     cy.get(onboardingLocators.timezoneDropdown).click()
     cy.get(commonLocators.dropdownOptions).contains("Eastern Time").click()

    // cy.get(onboardingLocators.ownernameField).type("QA")
    // cy.get(onboardingLocators.phoneField).type("1111111112")

    // cy.get(onboardingLocators.currencyDropdown).click()
    // cy.get(commonLocators.dropdownOptions).contains("US Dollar").click({force: true})
    //cy.wait(2000)
    cy.get(onboardingLocators.OnboardingActionsButtons).contains("Next").click()
    cy.get(onboardingLocators.OnboardingActionsButtons).contains("Back").click()
    cy.get(onboardingLocators.timezoneDropdown).click()
     cy.get(commonLocators.dropdownOptions).contains("Central Time").click()
     cy.get(onboardingLocators.OnboardingActionsButtons).contains("Next").click()
     // cy.getAfterValue(onboardingLocators.addressField, 'after', 'border-bottom-color').should("eq", "rgb(229, 57, 53)")

    // step 2
    // cy.wait(2000)
    cy.get(onboardingLocators.noOfMechanicsField).should("be.visible")
    // Move the focus to slider, by clicking on the slider's circle element
    cy.get(onboardingLocators.inputSlider).first().click({  force: true })
    // Press right arrow two times
    cy.get(onboardingLocators.inputSlider).first().type("{rightarrow}{rightarrow}")

    cy.get(onboardingLocators.inputSlider).last().click({  force: true })
    // Press right arrow two times
    cy.get(onboardingLocators.inputSlider).last().type("{rightarrow}{rightarrow}")

    // cy.get(onboardingLocators.noOfMechanicsField).type("10")
    // cy.get(onboardingLocators.noOfBaysField).type("5")

    // cy.get(onboardingLocators.ChargeForInspection).first().click()
    // cy.get(onboardingLocators.inspectionFeeField).type("10")

    // cy.get(onboardingLocators.chargeTax).last().click()
    // cy.get(onboardingLocators.setTaxField).type("20")
    //cy.wait(2000)
    cy.get(onboardingLocators.OnboardingActionsButtons).contains("Next").click()

    // step 3
    //cy.wait(2000)
    cy.get(onboardingLocators.featureAndOperationsCheckSpan).should("be.visible")

    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(4).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(5).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(7).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(2).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(8).click()

    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(14).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(15).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(17).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(12).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(18).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(24).click()
    // cy.get(onboardingLocators.featureAndOperationsCheckSpan).eq(26).click()

    // cy.get(onboardingLocators.customServicesField).type("extra service")
//  cy.wait(2000)
    cy.get(onboardingLocators.OnboardingActionsButtons).contains("Next").click()

    // step 4

    cy.get(onboardingLocators.vendorsField).should("be.visible")

    // cy.get(onboardingLocators.vendorsField).type("RepairDesk, Nexpart")

    // cy.get(onboardingLocators.avgMonRevenueField).clear().type("50000")

    // cy.get(onboardingLocators.oldManagement).eq(2).click()
    // cy.get(onboardingLocators.softwareUsedBeforeField).type("RD 1.0")
    cy.wait(2000)
    cy.get(commonLocators.submitButton).click()
    
    cy.wait(12000)
    cy.get(onboardingLocators.submitConfirmWizardButton).should("be.visible")

    // Adding mechanics
    cy.wait(2000)
    cy.get(onboardingLocators.submitConfirmWizardButton).contains("No, I will do that later").click()

    // cy.get(commonLocators.emailField).should("be.visible")
    
    // cy.get(addMechanicsLocators.accountType).eq(1).click()
    // cy.get(commonLocators.name).type("emp qa")
    // cy.get(commonLocators.emailField).type("qa.abdullah360+emp@gmail.com")

    // cy.get(commonLocators.submitButton).click()
    
    // cy.get(addMechanicsLocators.okInviteBtn).click()
    
    // cy.get(addMechanicsLocators.invitedUserList).contains("qa.abdullah360+emp@gmail.com").should("be.visible")

    // cy.get(addMechanicsLocators.closeButton).click()

    cy.get(addMechanicsLocators.addMechanicsConfirmButton).contains("Use Recommended").click()

})