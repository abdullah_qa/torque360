Feature: Inspection

    This Feature: contains all the test cases of Inspction CRUD options

    # Scenario: positive flow
    # When positive flow
    
    Scenario: User should not be allowed to Generate DVI without entering any field.
    Given the user is on New Inspections screen.
    Then the "General Inspection" button from "inspectionTypeBtns" should not be Clickable.
        And the "Detailed Inspection" button from "inspectionTypeBtns" should not be Clickable.
        And the "Generate DVI" button from "generateBtns" should not be Clickable.
        And the "Save Draft" button from "generateBtns" should not be Clickable.
        
    Scenario: User should NOT be allowed to Generate DVI by adding customer/vehicle but without general/detailed inspection.
    When the user selects "Mobile Number" value from the "searchByDropdown" dropdown.
        # And the user enters valid value in "searchCustomer".
    When the user selects customer and Vehicle, the data appear should be correct.
        # And the user store "customerData" data.
        # And the user hits the "qa" button from the "dropdownOptionButtons".
        # And the user store "vehicleData" data.
        # And the user hits the "Color:" button from the "dropdownVehicleButtons".
    # Then the customer should show correct "selectedcustomerData" data.
    #     And the customer should show correct "selectedvehicleData" data.
    When the user hits the "Generate DVI" button from the "generateBtns".
    Then the "Select atleast one inspection type." error should appear.

    # Todo: Customer and vehicle preview validations. 

    # Scenario: Customer and Vehicle Validations.
    # When the user enters valid value in "inspectionTitle".
    # Then the preview should show correct value for "inspectionTitle" against "Inspection Title".

    # Scenario: Customer and Vehicle Validations.
    # When the user enters valid value in "inspectionTitle".
    # Then the preview should show correct value for "inspectionTitle" against "Inspection Title".

    Scenario: Generate DVI by entering all fields.
    When the user hits the "General Inspection" button from the "inspectionTypeBtns".
        And the user adds "bad" condition details for "Headlights" part.
        And the user adds "good" condition details for "Mirrors" part.
        And the user adds "moderate" condition details for "Horn" part.
        # And the user adds "bad" condition details for "Fuel Tank Gasket" part.
        # And the user adds "moderate" condition details for "Door Locks" part.
        # And the user adds "bad" condition details for "Body Paint" part.
        And the user hits the "Under Vehicle" button from the "generalInspectionTabBtns".
        And the user adds "bad" condition details for "Suspension" part.
        # And the user adds "moderate" condition details for "Exhaust" part.
        # And the user adds "good" condition details for "Tyre /wheel LF" part.
        And the user adds "bad" condition details for "RF" part.
        And the user hits the "Under Hood" button from the "generalInspectionTabBtns".
        # And the user adds "bad" condition details for "Fluid Levels & Leaks" part.
        # And the user adds "moderate" condition details for "Drive Belts" part.
        And the user adds "good" condition details for "Coolant" part.
        And the user hits the "Electronics & Battery" button from the "generalInspectionTabBtns".
        # And the user adds "bad" condition details for "Battery Voltage" part.
        # And the user adds "moderate" condition details for "Instrument Cluster" part.
        And the user hits the "Save" button from the "dialogBtns".
    When the user hits the "Detailed Inspection" button from the "inspectionTypeBtns".
    And the user adds "bad" condition details for "Antenna" part.    
        And the user adds "good" condition details for "Belt-Timing" part.
        And the user adds "moderate" condition details for "Body-Paint Damage" part.
        # And the user adds "bad" condition details for "Interior Detailing" part.
        # And the user adds "moderate" condition details for "Bearing-Right Front" part.
        # And the user adds "bad" condition details for "Throw-outBearing-Noise" part.
        # And the user adds "bad" condition details for "Cooling System-Overheats" part.
        # And the user adds "moderate" condition details for "U Joint-Rear" part.
        And the user hits the "Save" button from the "dialogBtns".
    Then the parts with "Parts Need Atention" conditions should show correct inspection results.
        And the parts with "Immediate action required" conditions should show correct inspection results.

    Scenario: Fill all remaining fields that are optional. 
    When the user enters valid value in "inspectionTitle".
        And the user enters valid value in "diagnosticCode".
        And the user selects "fuelGauge" meter.
        And the user upload "valid_image.png" image for "fuelGaugeImageHolder".
        And the user enters valid value in "odoMeter".
        And the user upload "valid_image.png" image for "odoMeterImageHolder".
        And the user enters valid value in "extrasWithVehicle".
    Then the preview should show correct value for "inspectedBy" against "Inspected By".
        And the preview should show correct value for "inspectionTitle" against "Inspection Title".
        And the preview should show correct value for "searchCustomer" against "Customer Name".
        And the preview should show correct value for "vehicleName" against "Vehicle Name".
        And the preview should show correct value for "vehicleColor" against "Vehicle Color".
        And the preview should show correct value for "vehiclePlate" against "Vehicle Plate Naumber".
        And the user hits the "Generate DVI" button from the "generateBtns".
    Then the "signatureCanvas" field should appear.
        And the "authorizeBtn" field should not be Clickable.
    When the user draws signature. 
        And the user hits the "authorizeBtn" button.
    Then the "Digital Vehicle Inspection generated successfully!" success message should appear.
        And the user hits the "okBtn" button.

    Scenario: Generate DVI by adding customer/vehicle and without general/detailed inspection (Mandatory fields only).
    Given The user is on Dashboard.
    Given the user is on New Inspections screen.
    When the user selects "Customer Name" value from the "searchByDropdown" dropdown.
        # And the user enters valid value in "searchCustomer".
    When the user selects customer and Vehicle, the data appear should be correct.
    When the user hits the "General Inspection" button from the "inspectionTypeBtns".
    Then the "generalInspectionTabBtns" field should be visible.
    When the user adds "bad" condition details for "Headlights" part.
        And the user hits the "Save" button from the "dialogBtns".
        And the user hits the "Generate DVI" button from the "generateBtns".
    Then the "signatureCanvas" field should appear.
    When the user draws signature. 
        And the user hits the "authorizeBtn" button.
    Then the "Digital Vehicle Inspection generated successfully!" success message should appear.
        And the user hits the "okBtn" button.
