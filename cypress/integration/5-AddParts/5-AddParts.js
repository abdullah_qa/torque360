/// <reference types="Cypress" />

import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

const commonLocators = require("../../Locators/commonLocators.json")
const InspectionLocators = require("../../Locators/Inspection.json")
const AddPartsLocators = require("../../Locators/AddPartsLocators.json")

function getNextNumber(previousEmail) {
    let halfEmail = previousEmail.split("+")[1]
    let uniqueNumber = halfEmail.split("@")[0]
    uniqueNumber = parseInt(uniqueNumber) 
    return uniqueNumber
}

Given("the user is on New Jobcard screen.", () => {
    cy.visit("/jobcard")
    cy.get("h4").should("contain", "New Jobcard")
    // cy.get(commonLocators.navNewActivityBtn).click( {force: true} )
    // cy.get(commonLocators.navOptions).contains("Jobcard").click({ force: true })
})

When("the user selects {string} value from the {string} dropdown.", (option, dropdown) => {
    cy.fixture('parts_examples').then(data => {
        cy.get(AddPartsLocators[dropdown]).click( {force: true} )
        cy.get(commonLocators.dropdownOptions).contains(option).click( {force: true} )
    })
})

Then("the {string} field should be visible.", (field) => {
    cy.get(AddPartsLocators[field]).should("be.visible")
})

Then("the {string} button from {string} should not be Clickable.", (btn, buttonGroup) => {
    cy.get(AddPartsLocators[buttonGroup]).contains(btn).parent().should("be.disabled")
})

Then("the {string} field with text {string} should be visible.", (field, fieldText) => {
    cy.get(AddPartsLocators[field]).should("have.text", fieldText)
})

When("the user check the {string} checkbox.", (field) => {
    cy.get(AddPartsLocators[field]).check().should("be.checked")
})

When("the user hits the {string} button.", (field) => {
    cy.get(AddPartsLocators[field]).click( {force: true} )
})

When("the user hits the {string} button from the {string}.", (btn, buttonGroup) => {
    cy.get(AddPartsLocators[buttonGroup]).contains(new RegExp("^" + btn + "$", "g")).click( {force: true} )
})

When("the user upload {string} for {string}.", (image, imageHolder) => {
    cy.get(AddPartsLocators[imageHolder]).attachFile(image)
    cy.get(AddPartsLocators[imageHolder]).parent().next().should("not.have.a.property", "style")
})

Then("the {string} error appears under profile image.", (errorMessage) => {
    cy.get(AddPartsLocators.errorMessage).should("have.text", errorMessage)
})

When("the user enters valid value in {string} at {string} screen.", (field, fieldsType) => {
    cy.fixture('parts_examples').then(data => {
        cy.get(AddPartsLocators[fieldsType][field]).clear()
        cy.get(AddPartsLocators[fieldsType][field]).type(data.newData[fieldsType][field])
    })
})

When("the user enters invalid value in {string}.", (field) => {
    cy.fixture('parts_examples').then(data => {
        cy.get(AddPartsLocators[field]).clear()
        cy.get(AddPartsLocators[field]).type(data[field])
    })
})

When("the user enters space in {string}.", (field) => {
    cy.get(AddPartsLocators[field]).type("  ")
})

When("the user enters no value in {string}.", (field) => {
    cy.get(AddPartsLocators[field]).clear()
})

When("the user selects {string} value from the {string} dropdown.", (option, dropdown) => {
    cy.fixture('parts_examples').then(data => {
        cy.get(AddPartsLocators[dropdown]).click( {force: true} )
        cy.get(commonLocators.dropdownOptions).contains(option).click( {force: true} )
    })
})

When("the user selects no value from the {string}.", (dropdown) => {
    // cy.fixture('onboarding_examples').then(data => {
    //     cy.get(onboardingLocators[dropdown]).clear()
    // })
    // pass
})

When("the user chooses {string} for {string} options.", (option, group) => {
    cy.get(onboardingLocators.allRadioOptions).each(($col, index, $list) => {
        let radioText = $col.text()
        if (radioText === option) {
            cy.get($col).prev().find(onboardingLocators.radioButton).click( {force: true} )
        }
    })
})

Then("the {string} field should appear.", (field) => {
    cy.get(AddPartsLocators[field]).should("be.visible")
})

Then("the {string} error should appear.", (errorMessage) => {
    cy.get(AddPartsLocators.inspectionTypeError).should("have.text", errorMessage)
})

Then("the {string} should appear.", (message) => {
    // Reading fixture file.
    cy.fixture('parts_examples').then(data => {
        if (message == "Information added successfully" || message == "Vehicle added successfully") {
            cy.get(commonLocators.customerToastMessage).should("have.text", message)

            // Adding new data to the customer example for the next time.
            cy.getUniqueName(getNextNumber(data.newData.Customer.customerEmail)).then(newName => {
                data.newData.Customer.name = newName

                data.newData.Customer.phone = parseInt(data.newData.Customer.phone) + 1

                // get Unique Email.
                cy.getUniqueEmail(data.newData.Customer.customerEmail).then(newEmail => {
                    data.newData.Customer.customerEmail = newEmail

                    cy.getUniqueVehiclePlate(data.newData.Vehicle.vehiclePlate).then(newVehiclePlate => {
                        data.newData.Vehicle.vehiclePlate = newVehiclePlate

                        cy.writeFile("cypress/fixtures/parts_examples.json", JSON.stringify(data))
                    })
                })
            })            
        } else {
            cy.get(commonLocators.customerToastMessage).should("have.text", message)
        }
    })
})

Then("the {string} success message should appear.", (message) => {
    cy.get(commonLocators.customerToastMessage).should("have.text", message)
})

Then("the {string} field at {string} section, should be underlined as red.", (field, fieldSet) => {
    cy.getAfterValue(AddPartsLocators[fieldSet][field], 'after', 'border-bottom-color').should("eq", "rgb(229, 57, 53)")
})

Then("the {string} field should NOT be underlined as red.", (field) => {
    cy.getAfterValue(AddPartsLocators[field], 'after', 'border-bottom-color').should("eq", "rgb(105, 33, 196)")
})

Then("all fields of the {string} screen should be visible.", async (screen) => {
    for(let d in AddPartsLocators[screen]) {
        cy.get(AddPartsLocators[screen][d]).should("exist")
    }
})

When("the user fills all fields of {string} with {string}.", async (fields, fieldsType) => {
    let locators = AddPartsLocators[fields]
    cy.fixture("parts_examples").then(addPartsData => {
        let data = addPartsData[fieldsType][fields]
        for(let loc in data) {
            if(loc.includes("Check")){
                cy.get(locators[loc]).check().should("be.checked")
            } else if (loc.includes("Dropdown")) {
                cy.get(locators[loc]).click( {force: true} )
                cy.get(commonLocators.dropdownOptions).contains(data[loc]).click( {force: true} )
            } else if (loc.includes("Btn")) {
                cy.get(locators[loc]).click( {force: true} )
            } else if (loc.includes("Radio")) {
                cy.get(locators[loc]).check(data[loc])
            } else if (loc.includes("ImageHolder")) {
                // Issue needed to be fixed.
            } else {
                cy.get(locators[loc]).clear()
                cy.get(locators[loc]).type(data[loc])
                
                // condition will be updated when the phone, date fields error will be removed.

                if (fieldsType == "exceedingCharaterLimits" || fieldsType == "invalidData") {
                    cy.get(locators[loc]).parent().parent().find(commonLocators.validationError).invoke("text").then(errorText => {
                        expect(addPartsData.errorMessages[fieldsType]).to.be.include(errorText)
                        if (fieldsType == "exceedingCharaterLimits" ) {
                            cy.get(locators[loc]).type("{backspace}")
                        } else {
                            cy.get(locators[loc]).clear()
                            cy.get(locators[loc]).type(addPartsData["validData"][fields][loc])
                        }

                        cy.get(locators[loc]).parent().parent().find(commonLocators.validationError).should("not.exist")
                    })
                }
            }
        }
    })
})

Then("All data of the {string} should be correct.", (fields) => {
    let locators = AddPartsLocators[fields]
    cy.fixture("parts_examples").then(addPartsData => {
        let data = addPartsData["validData"][fields]
        for(let loc in locators) {
            if(loc.includes("Check")){
                // Wating for the tax toggle fix.
                // cy.get(locators[loc]).should("be.checked")
            } else if (loc.includes("Dropdown")) {
                cy.get(locators[loc]).next().should("have.value", data[loc])
            } else if (loc.includes("phone")) {
                cy.get(locators[loc]).should("have.value", "+1 (111) 111-1111")
            } else if (loc.includes("Btn")) {
                // cy.get(locators[loc]).click( {force: true} )
            } else if (loc.includes("Radio")) {
                // cy.get(locators[loc]).check(data[loc])
            } else {
                cy.get(locators[loc]).should("have.value", data[loc])
                // cy.get(locators[loc]).clear()
            }
        }
    })
})
