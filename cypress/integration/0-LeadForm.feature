Feature: Lead Form

    Background: 
        Given Go to branding site and Open Lead form
    
    Scenario: Fill form with all blank fields
    When the user clicks Submit button
    Then the "Name Required" validation error should appear.
        And the "Email Required" validation error should appear.
        And the "Phone Number Required " validation error should appear against phone number field.
        And the "Name Required" validation error should appear.

    Scenario: Fill form with blank name field.
    When the user enters website: "www.oldcrm.com"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Name Required" validation error should appear.
    When the user enters valid Name: "  "
        And the user clicks Submit button
    Then the "Name Required" validation error should appear.

    Scenario: Fill form with invalid name field.
    When the user enters valid Name: "SQAE#1@$"
        And the user enters website: "www.oldcrm.com"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Special Character Not Allowed" validation error should appear.

    Scenario: Fill form with blank Website field.
    When the user enters valid Name: "SQAE"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Website Required" validation error should appear.
    When the user enters website: "  "
        And the user clicks Submit button
    Then the "Invalid URL" validation error should appear.

    Scenario: Fill form with invalid Website field.
    When the user enters valid Name: "SQAE"
        And the user enters website: "wwwldcom"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Invalid URL" validation error should appear.

    Scenario: Fill form with blank phone field.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters "validEmail" in Email field
        And the user clicks Submit button
    Then the "Phone Number Required " validation error should appear against phone number field.
    When the user enters phone: "  "
        And the user clicks Submit button
    Then the "Invalid Phone Number " validation error should appear against phone number field.

    Scenario: Fill form with invalid phone field.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters "validEmail" in Email field
        And the user enters phone: "54525323"
        And the user clicks Submit button
    Then the "Invalid Phone Number " validation error should appear against phone number field.

    Scenario: Fill form with blank email field.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Email Required" validation error should appear.
    When the user enters "BlankEmail" in Email field
        And the user clicks Submit button
    Then the "Invalid Email Address" validation error should appear.

    Scenario: Fill form with invalid email field.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters "InValidEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "Invalid Email Address" validation error should appear.

    Scenario: Fill form with already existing email fields.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters "AlreadyExistingEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "AlreadyExistingEmailError" should appear.

    Scenario: Fill form with all valid fields.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Submit button
    Then the "SuccessMessage" should appear.

    Scenario: Fill form but close the lead form popup.
    When the user enters valid Name: "SQAE"
        And the user enters website: "www.oldcrm.com"
        And the user enters "validEmail" in Email field
        And the user enters phone: "5452345323"
        And the user clicks Close button
    Then the form should not be visible.