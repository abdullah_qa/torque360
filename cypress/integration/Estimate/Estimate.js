/// <reference types="Cypress" />

import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"
import 'cypress-file-upload'

const commonLocators = require("../../Locators/commonLocators.json")
const InspectionLocators = require("../../Locators/Inspection.json")
const EstimateLocators = require("../../Locators/EstimateLocators.json")

function getWholeArrayAsString (arrayString) {
    if (arrayString.includes(",")){
        arrayString = arrayString.replaceAll(",", ", ")
    }
    arrayString = arrayString.replaceAll("[", "")
    arrayString = arrayString.replaceAll("]", "")
    arrayString = arrayString.replaceAll("\"", "")
    cy.log(arrayString)
    return arrayString
}

Given("the user is on New Estimate screen.", () => {
    // cy.get(commonLocators.navNewActivityBtn).click()
    // cy.get(commonLocators.navOptions).contains("Estimate").click({ force: true })
    cy.get(EstimateLocators.otherJobType).should("be.visible")
})

Then("the {string} field should be visible.", (field) => {
    cy.get(EstimateLocators[field]).should("be.visible")
})

Then("the {string} button from {string} should not be Clickable.", (btn, buttonGroup) => {
    cy.get(EstimateLocators[buttonGroup]).contains(btn).parent().should("be.disabled")
})

Then("the {string} field should not be Clickable.", (btn) => {
    cy.get(EstimateLocators[btn]).should("be.disabled")
})

Then("the {string} field with text {string} should be visible.", (field, fieldText) => {
    cy.get(EstimateLocators[field]).should("have.text", fieldText)
})

When("the user hits the {string} button.", (field) => {
    cy.get(EstimateLocators[field]).click()
})

When("the user hits the {string} button from the {string}.", (btn, buttonGroup) => {
    cy.get(EstimateLocators[buttonGroup]).contains(btn).click()
})

When("the user selects {string} meter.", (field) => {
    cy.fixture('estimate_examples').then(data => {
        cy.get(EstimateLocators[field]).eq(data[field]).click()

    })
})
When("the user upload {string} image for {string}.", (image, imageHolder) => {
    cy.get(EstimateLocators[imageHolder]).attachFile(image)
    // cy.get(EstimateLocators[imageHolder]).next().children().first().should("have.a.property", "style")
})

Then("the {string} error appears under profile image.", (errorMessage) => {
    cy.get(EstimateLocators.errorMessage).should("have.text", errorMessage)
})

When("the user enters valid value in {string}.", (field) => {
    cy.fixture('estimate_examples').then(data => {
        cy.get(EstimateLocators[field]).clear()
        cy.get(EstimateLocators[field]).type(data[field])
    })
})

When("the user enters invalid value in {string}.", (field) => {
    cy.fixture('estimate_examples').then(data => {
        cy.get(EstimateLocators[field]).clear()
        cy.get(EstimateLocators[field]).type(data[field])
    })
})

When("the user enters space in {string}.", (field) => {
    cy.get(EstimateLocators[field]).type("  ")
})

When("the user enters no value in {string}.", (field) => {
    cy.get(EstimateLocators[field]).clear()
})

When("the user selects estimated {string}.", (dropdown) => {
    cy.fixture('estimate_examples').then(data => {
        cy.get(EstimateLocators[dropdown]).click()
        cy.get(commonLocators.dropdownOptions).contains(data[dropdown]).click()
    })
})

When("the user selects {string} value from the {string} dropdown.", (option, dropdown) => {
    cy.fixture('estimate_examples').then(data => {
        cy.get(EstimateLocators[dropdown]).click()
        cy.get(commonLocators.dropdownOptions).contains(option).click()
    })
})

When("the user selects no value from the {string}.", (dropdown) => {
    // cy.fixture('onboarding_examples').then(data => {
    //     cy.get(onboardingLocators[dropdown]).clear()
    // })
    // pass
})

When("the user selects {string}.", (field) => {
    cy.fixture('estimate_examples').then(data => {
        for (var job in data[field]){
            cy.get(EstimateLocators[field]).contains(data[field][job]).click()
        }
    })
})

Then("the {string} field should appear.", (field) => {
    cy.get(EstimateLocators[field]).should("be.visible")
})

Then("the {string} error should appear.", (errorMessage) => {
    cy.get(EstimateLocators.inspectionTypeError).should("have.text", errorMessage)
})

Then("the {string} success message should appear.", (message) => {
    cy.get(EstimateLocators.successTitle).should("have.text", message)
    // cy.fixture('estimate_examples').then(data => {
    //     cy.getUniqueName(data.inspectionTitle).then(newInspectionTitle => {
    //         data.inspectionTitle = newInspectionTitle
    //         cy.writeFile("cypress/fixtures/estimate_examples.json", JSON.stringify(data))
    //     })    
    // })
            
})

When("the user selects customer and Vehicle, the data appear should be correct.", async () => {
    cy.fixture("customer_examples").then(customerVehicleData => {
        let data = customerVehicleData.validData
        cy.get(EstimateLocators.searchCustomer).clear()
        cy.get(EstimateLocators.searchCustomer).type(data.Customer.phone)

        cy.get(EstimateLocators.dropdownOptionButtons).should("be.visible")
        // Storing the customer data
        cy.get(EstimateLocators.customerData).contains(data.Customer.phone).parents(EstimateLocators.customerData).invoke("text").then(fieldText => {
            let expectedCustomerData = fieldText.replace(/ /g, '')
            cy.log(expectedCustomerData)

            // Selecting customer
            cy.get(EstimateLocators.dropdownOptionButtons).contains(data.Customer.phone).click()

            // Storing the Vehicle data
            cy.get(EstimateLocators.vehicleData).contains(data.Vehicle.vehiclePlate).parents(EstimateLocators.vehicleData).invoke("text").then(fieldText => {
                let expectedVehicleData = fieldText.replace(/ /g, '') // qa
                cy.log(expectedVehicleData)

                // Selecting vehicle
                cy.get(EstimateLocators.vehicleData).contains(data.Vehicle.vehiclePlate).click()

                // Asserting the Customer data
                cy.get(EstimateLocators.selectedcustomerData).first().invoke("text").then(fieldText => {
                    let observedCustomerData = fieldText.replace(/ /g, '') // qa
                    cy.log(observedCustomerData)
                    expect(expectedCustomerData).to.be.equal(observedCustomerData)

                    // Asserting the Vehicle data
                    cy.get(EstimateLocators.selectedvehicleData).last().invoke("text").then(fieldText => {
                        let observedVehicleData = fieldText.replace(/ /g, '') // qa
                        cy.log(observedVehicleData)
                        // Will Enable when the issue fixed
                        // expect(expectedVehicleData).to.be.equal(observedVehicleData)
                    })

                })

            })

        })
    })

})

When("the user selects all the problems appears in dropdown.", () => {
    let problemArray = []
    cy.get(EstimateLocators.problems).each((p, i, list) => {
        cy.wrap(p).click()
        problemArray[i] = p.text()

        cy.get(EstimateLocators.notes).should("have.text", getWholeArrayAsString(JSON.stringify(problemArray)))
    })
})

When("the user unselects few selected problems appears in dropdown.", () => {
    cy.get(EstimateLocators.problemSearch).clear()
    cy.get(EstimateLocators.problemSearch).type("cam")
    cy.get(EstimateLocators.problems).each((p, i, list) => {
        cy.wrap(p).click()

        cy.get(EstimateLocators.notes).should("not.have.text", p.text())
    })
})

When("the user store {string} data.", (cv) => {
    cy.get(EstimateLocators.dropdownOptionButtons).should("be.visible")
    cy.get(EstimateLocators[cv]).invoke("text").then(fieldText => {
        let txt = fieldText.replace(/ /g, '') // qa
        cy.log(txt)
        cy.readFile("cypress/fixtures/estimate_examples.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then((data) => {
            data["searched" + cv] = txt
            cy.writeFile("cypress/fixtures/estimate_examples.json", JSON.stringify(data))
        })
    })
})

Then("the customer should show correct {string} data.", (cv) => {
    let n = 0
    if (cv === "selectedvehicleData") {
        n = 1
    }

    cy.wait(15000)
    cy.get(EstimateLocators[cv]).eq(n).invoke("text").then(fieldText => {
        let txt = fieldText.replace(/ /g, '') // qa
        cy.log(txt)
        cy.fixture("onboarding_examples").then(data => {
            expect(txt).to.equal(data["letsBeginMessage"])
        })
    })

})

Then("the preview should show correct value for {string} against {string}.", (field, previewFieldName) => {
    cy.fixture("estimate_examples").then(data => {
        let expectedValue = data[field]
        cy.get(EstimateLocators.previewField).contains(previewFieldName).parent().find(EstimateLocators.previewFieldValue).should("have.text", expectedValue)

    })
})

When("positive flow", () => {

    cy.get(commonLocators.navNewActivityBtn).click()
    cy.get(commonLocators.navOptions).contains("Inspection").click({ force: true })

    cy.get(EstimateLocators.addCustomerBtn).contains("Add Customer").should("be.visible")

    // Create customer
    // cy.get(EstimateLocators.addCustomerBtn).contains("Add Customer").click()


    cy.get(EstimateLocators.searchByDropdown).click()
    cy.get(commonLocators.dropdownOptions).contains("Customer Name").click()
    cy.get(EstimateLocators.searchCustomer).type("qa")

    cy.get(EstimateLocators.dropdownOptionButtons).contains("qa").click()
    cy.get("span").contains("Color:").click()

    cy.get(EstimateLocators.inspectionTypeBtns).contains("General Inspection Template").click()

    cy.get(EstimateLocators.radioGroup).eq(5).find(EstimateLocators.badCondition).click({ force: true })
    cy.get(EstimateLocators.radioGroup).eq(7).find(EstimateLocators.ModerateCondition).click({ force: true })

    cy.get(commonLocators.dialogBtns).contains("Save").click()

    cy.wait(2000)
    cy.get(EstimateLocators.generateBtns).contains("Generate DVI").click()
    cy.get(EstimateLocators.signatureCanvas).should("be.visible")

    cy.get(EstimateLocators.signatureCanvas).click({ multiple: true }, { force: true }).type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}")
    cy.get(EstimateLocators.authorizeBtn).click()

    cy.get(EstimateLocators.successTitle).should("have.text", "Inspection has been created")
})