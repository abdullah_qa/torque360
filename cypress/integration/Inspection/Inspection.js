/// <reference types="Cypress" />

import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"
import 'cypress-file-upload'

const commonLocators = require("../../Locators/commonLocators.json")
const dashboardLocators = require("../../Locators/dashboard.json")
const SignInLocators = require("../../Locators/SignIn.json")
const InspectionLocators = require("../../Locators/Inspection.json")

function doSignature(selector, x, y) {
    cy.get(selector)
        .trigger("mousemove", 5, 5, { force: true })
        .trigger("mousedown", { button: 0 }, { force: true })
        .trigger("mousemove", x, y, { force: true })
    cy.get(InspectionLocators.signatureCanvas)
        .trigger("mousemove", x, y)
        .trigger("mouseup", x, y + 20)
}

Given("the user is on New Inspections screen.", () => {
    cy.get(commonLocators.navNewActivityBtn).click()
    cy.get(commonLocators.navOptions).contains("Inspection").click({ force: true })
    cy.get(InspectionLocators.addCustomerBtn).contains("Add Customer").should("be.visible")
})

Then("the {string} field should be visible.", (field) => {
    cy.get(InspectionLocators[field]).should("be.visible")
})

Then("the {string} button from {string} should not be Clickable.", (btn, buttonGroup) => {
    cy.get(InspectionLocators[buttonGroup]).contains(btn).parent().should("be.disabled")
})

Then("the {string} field should not be Clickable.", (btn) => {
    cy.get(InspectionLocators[btn]).should("be.disabled")
})

Then("the {string} field with text {string} should be visible.", (field, fieldText) => {
    cy.get(InspectionLocators[field]).should("have.text", fieldText)
})

When("the user hits the {string} button.", (field) => {
    cy.get(InspectionLocators[field]).click()
})

When("the user hits the {string} button from the {string}.", (btn, buttonGroup) => {
    cy.get(InspectionLocators[buttonGroup]).contains(btn).click()
})

When("the user selects {string} meter.", (field) => {
    cy.fixture('inspection_examples').then(data => {
        cy.get(InspectionLocators[field]).eq(data[field]).click()

    })
})
When("the user upload {string} image for {string}.", (image, imageHolder) => {
    cy.get(InspectionLocators[imageHolder]).attachFile(image)
    // cy.get(InspectionLocators[imageHolder]).next().children().first().should("have.a.property", "style")
})

Then("the {string} error appears under profile image.", (errorMessage) => {
    cy.get(InspectionLocators.errorMessage).should("have.text", errorMessage)
})

When("the user enters valid value in {string}.", (field) => {
    cy.fixture('inspection_examples').then(data => {
        cy.get(InspectionLocators[field]).clear()
        cy.get(InspectionLocators[field]).type(data[field])
    })
})

When("the user enters invalid value in {string}.", (field) => {
    cy.fixture('inspection_examples').then(data => {
        cy.get(InspectionLocators[field]).clear()
        cy.get(InspectionLocators[field]).type(data[field])
    })
})

When("the user enters space in {string}.", (field) => {
    cy.get(InspectionLocators[field]).type("  ")
})

When("the user enters no value in {string}.", (field) => {
    cy.get(InspectionLocators[field]).clear()
})

When("the user selects {string} value from the {string} dropdown.", (option, dropdown) => {
    cy.fixture('inspection_examples').then(data => {
        cy.get(InspectionLocators[dropdown]).click()
        cy.get(commonLocators.dropdownOptions).contains(option).click()
    })
})

When("the user selects no value from the {string}.", (dropdown) => {
    // cy.fixture('onboarding_examples').then(data => {
    //     cy.get(onboardingLocators[dropdown]).clear()
    // })
    // pass
})

When("the user add total {string} out of {string} through the slider.", (slider, maxValue) => {
    cy.get(onboardingLocators.inputSlider + "[aria-valuemax=" + maxValue + "]").click({ force: true })
    cy.get(onboardingLocators.inputSlider + "[aria-valuemax=" + maxValue + "]").type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}")
})

When("the user chooses {string} for {string} options.", (option, group) => {
    cy.get(onboardingLocators.allRadioOptions).each(($col, index, $list) => {
        let radioText = $col.text()
        if (radioText === option) {
            cy.get($col).prev().find(onboardingLocators.radioButton).click()
        }
    })
})

Then("the {string} field should appear.", (field) => {
    cy.get(InspectionLocators[field]).should("be.visible")
})

Then("the {string} error should appear.", (errorMessage) => {
    cy.get(InspectionLocators.inspectionTypeError).should("have.text", errorMessage)
})

Then("the {string} success message should appear.", (message) => {
    cy.get(InspectionLocators.successTitle).should("have.text", message)
    cy.fixture('inspection_examples').then(data => {
        cy.getUniqueName(data.inspectionTitle).then(newInspectionTitle => {
            data.inspectionTitle = newInspectionTitle
            cy.writeFile("cypress/fixtures/inspection_examples.json", JSON.stringify(data))
        })    
    })
            
})

When("the user selects customer and Vehicle, the data appear should be correct.", async () => {
    cy.fixture("customer_examples").then(customerVehicleData => {
        let data = customerVehicleData.validData
        cy.get(InspectionLocators.searchCustomer).clear()
        cy.get(InspectionLocators.searchCustomer).type(data.Customer.phone)

        cy.get(InspectionLocators.dropdownOptionButtons).should("be.visible")
        // Storing the customer data
        cy.get(InspectionLocators.customerData).invoke("text").then(fieldText => {
            let expectedCustomerData = fieldText.replace(/ /g, '')
            cy.log(expectedCustomerData)

            // Selecting customer
            cy.get(InspectionLocators.dropdownOptionButtons).contains(data.Customer.phone).click()

            // Storing the Vehicle data
            cy.get(InspectionLocators.vehicleData).contains(data.Vehicle.vehiclePlate).parents(InspectionLocators.vehicleData).invoke("text").then(fieldText => {
                let expectedVehicleData = fieldText.replace(/ /g, '') // qa
                cy.log(expectedVehicleData)

                // Selecting vehicle
                cy.get(InspectionLocators.vehicleData).contains(data.Vehicle.vehiclePlate).click()

                // Asserting the Customer data
                cy.get(InspectionLocators.selectedcustomerData).first().invoke("text").then(fieldText => {
                    let observedCustomerData = fieldText.replace(/ /g, '') // qa
                    cy.log(observedCustomerData)
                    expect(expectedCustomerData).to.be.equal(observedCustomerData)

                    // Asserting the Vehicle data
                    cy.get(InspectionLocators.selectedvehicleData).last().invoke("text").then(fieldText => {
                        let observedVehicleData = fieldText.replace(/ /g, '') // qa
                        cy.log(observedVehicleData)
                        // Will Enable when the issue fixed
                        expect(expectedVehicleData).to.be.equal(observedVehicleData)
                    })

                })

            })

        })
    })

})

When("the user store {string} data.", (cv) => {
    cy.get(InspectionLocators.dropdownOptionButtons).should("be.visible")
    cy.get(InspectionLocators[cv]).invoke("text").then(fieldText => {
        let txt = fieldText.replace(/ /g, '') // qa
        cy.log(txt)
        cy.readFile("cypress/fixtures/inspection_examples.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then((data) => {
            data["searched" + cv] = txt
            cy.writeFile("cypress/fixtures/inspection_examples.json", JSON.stringify(data))
        })
    })
})

Then("the customer should show correct {string} data.", (cv) => {
    let n = 0
    if (cv === "selectedvehicleData") {
        n = 1
    }

    cy.wait(15000)
    cy.get(InspectionLocators[cv]).eq(n).invoke("text").then(fieldText => {
        let txt = fieldText.replace(/ /g, '') // qa
        cy.log(txt)
        cy.fixture("onboarding_examples").then(data => {
            expect(txt).to.equal(data["letsBeginMessage"])
        })
    })

})

When("the user adds {string} condition details for {string} part.", (condition, part) => {
    cy.get(InspectionLocators.partName)
        .contains(part)
        .parent()
        .find(InspectionLocators.partNotes).focus().type("The " + part + " is in " + condition + " condition.")

    cy.get(InspectionLocators.partName)
        .contains(part)
        .parent()
        .find(InspectionLocators.partImageHolder).attachFile(condition + ".png")

    cy.get(InspectionLocators.partName)
        .contains(part)
        .parent()
        .find("[value='" + condition + "']").click({ force: true })

    // cy.readFile("cypress/fixtures/inspection_examples.json").then((data) => {
    //     var count = Object. keys(data.partsCondition). length;
    //     data.partsCondition[count] = condition + part // i.e. bad Mirrors
    //     cy.writeFile("cypress/fixtures/inspection_examples.json", JSON.stringify(data))
    // })
})

When("the parts with {string} conditions should show correct inspection results.", (conditionType) => {
    let condition = "Bad"
    if (conditionType === "Parts Need Atention") {
        condition = "Moderate"
    }
    cy.fixture("inspection_examples").then(data => {
        cy.get(InspectionLocators.previewConditionName).contains(conditionType).click()
        cy.get(InspectionLocators["preview" + condition + "ConditionPartsName"]).each((col, index, $list) => {
            let partName = col.text()
            expect(data.partsCondition).to.include(condition + partName)
            if ($list.length == (index + 1)) {
                cy.log("Asserting Total inspected problems.")
                cy.get(InspectionLocators["preview" + condition + "ConditionPartsName"]).its("length").should("eq", $list.length)
                cy.get(InspectionLocators.previewConditionName).contains(conditionType).next().should("have.text", $list.length)
            }
        })

        // Hide the condition section
        cy.get(InspectionLocators.previewConditionName).contains(conditionType).click()
    })
})

Then("the preview should show correct value for {string} against {string}.", (field, previewFieldName) => {
    cy.fixture("inspection_examples").then(data => {
        let expectedValue = data[field]
        cy.get(InspectionLocators.previewField).contains(previewFieldName).parent().find(InspectionLocators.previewFieldValue).should("have.text", expectedValue)

    })
})

When("the user draws signature.", () => {
    cy.get(InspectionLocators.signatureCanvas)
        .click({ multiple: true }, { force: true })
        .type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}")
})

When("positive flow", () => {

    cy.get(commonLocators.navNewActivityBtn).click()
    cy.get(commonLocators.navOptions).contains("Inspection").click({ force: true })

    cy.get(InspectionLocators.addCustomerBtn).contains("Add Customer").should("be.visible")

    // Create customer
    // cy.get(InspectionLocators.addCustomerBtn).contains("Add Customer").click()


    cy.get(InspectionLocators.searchByDropdown).click()
    cy.get(commonLocators.dropdownOptions).contains("Customer Name").click()
    cy.get(InspectionLocators.searchCustomer).type("qa")

    cy.get(InspectionLocators.dropdownOptionButtons).contains("qa").click()
    cy.get("span").contains("Color:").click()

    cy.get(InspectionLocators.inspectionTypeBtns).contains("General Inspection Template").click()

    cy.get(InspectionLocators.radioGroup).eq(5).find(InspectionLocators.badCondition).click({ force: true })
    cy.get(InspectionLocators.radioGroup).eq(7).find(InspectionLocators.ModerateCondition).click({ force: true })

    cy.get(commonLocators.dialogBtns).contains("Save").click()

    cy.wait(2000)
    cy.get(InspectionLocators.generateBtns).contains("Generate DVI").click()
    cy.get(InspectionLocators.signatureCanvas).should("be.visible")

    cy.get(InspectionLocators.signatureCanvas).click({ multiple: true }, { force: true }).type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}")
    cy.get(InspectionLocators.authorizeBtn).click()

    cy.get(InspectionLocators.successTitle).should("have.text", "Inspection has been created")
})