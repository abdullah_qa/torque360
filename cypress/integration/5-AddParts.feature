Feature: Add Parts/Services/Labor/Canned Services.

    This Feature: contains all the test cases of Add Parts/Services/Labor/Canned Services CRUD options

    Scenario: Parts - All fields of the Parts should be visible.
    Given the user is on New Jobcard screen.
        And the user hits the "Add Parts" button from the "addPartsBtns".
        And the user hits the "Add New Part" button from the "addNewPartBtns".
    Then all fields of the "Parts" screen should be visible.

    Scenario: Parts - Validate all fields of create parts without filling any field.
    When the user hits the "Add" button from the "addNewPartBtns".
    Then the "partCategoryDropdown" field at "Parts" section, should be underlined as red.
        And the "partTypeDropdown" field at "Parts" section, should be underlined as red.
        And the "partName" field at "Parts" section, should be underlined as red.
        And the "sku" field at "Parts" section, should be underlined as red.

    Scenario: Parts - Validate all valid fields of create Parts with invalid Data.
    When the user fills all fields of "Parts" with "invalidData".

    Scenario: Parts - Validate all fields of create Parts with exceeding Charater Limits.
    When the user fills all fields of "Parts" with "exceedingCharaterLimits".

    Scenario: Parts - Validate all fields of create new part with valid data.
    When the user fills all fields of "Parts" with "validData".
        # And the user hits the "Add" button from the "addNewPartBtns".
        And the user hits the "Back" button from the "addNewPartBtns". 

    Scenario: Labor - All fields of the Labor should be visible.
    Given the user is on New Jobcard screen.
    When the user hits the "Add Labor" button from the "addPartsBtns".
        And the user hits the "Add New Labor" button from the "addNewPartBtns".
    Then all fields of the "Labor" screen should be visible.

    Scenario: Labor - Validate all fields of create Labor without filling any field.
    When the user hits the "Add" button from the "addNewPartBtns".
    Then the "category" field at "Labor" section, should be underlined as red.

    Scenario: Labor - Validate all valid fields of create Labor with invalid Data.
    When the user fills all fields of "Labor" with "invalidData".

    Scenario: Labor - Validate all fields of create Labor with exceeding Charater Limits.
    When the user fills all fields of "Labor" with "exceedingCharaterLimits".

    Scenario: Labor - Validate all fields of create new Labor with valid data.
    When the user fills all fields of "Labor" with "validData".
        And the user hits the "Back" button from the "addNewPartBtns".

    Scenario: Supplies - All fields of the Supplies should be visible.
    Given the user is on New Jobcard screen.
    When the user hits the "Add Supplies" button from the "addPartsBtns".
        And the user hits the "Add New Item" button from the "addNewPartBtns".
    Then all fields of the "Supplies" screen should be visible.

    Scenario: Supplies - Validate all fields of create Supplies without filling any field.
    When the user hits the "Add" button from the "addNewPartBtns".
    Then the "brand" field at "Supplies" section, should be underlined as red.
        And the "item" field at "Supplies" section, should be underlined as red.
        And the "sku" field at "Supplies" section, should be underlined as red.

    Scenario: Supplies - Validate all valid fields of create Supplies with invalid Data.
    When the user fills all fields of "Supplies" with "invalidData".

    Scenario: Supplies - Validate all fields of create Parts with exceeding Charater Limits.
    When the user fills all fields of "Supplies" with "exceedingCharaterLimits".

    Scenario: Supplies - Validate all fields of create new Supplies with valid data.
    When the user fills all fields of "Supplies" with "validData".
# Todo
    # Scenario: Validate all fields of create customer/vehicle Without filling any field.
    # When the user hits the "Save" button from the "footerButtons".
    # Then the "name" field at "Customer" section, should be underlined as red.
    #     And the "customerEmail" field at "Customer" section, should be underlined as red.
    #     # And the "phone" field at "Customer" section, should be underlined as red.
    #     And the "vehicleModelYear" field at "Vehicle" section, should be underlined as red.
    #     And the "vehicleModel" field at "Vehicle" section, should be underlined as red.
    #     And the "vehicleMake" field at "Vehicle" section, should be underlined as red.
    #     And the "vehiclePlate" field at "Vehicle" section, should be underlined as red.
    #     And the "vehicleFuelTypeDropdown" field at "Vehicle" section, should be underlined as red.
    #     And the "vehicleColor" field at "Vehicle" section, should be underlined as red.

#     Scenario: Validate all valid fields of create customer/vehicle with invalid Data.
#     When the user fills all fields of "Customer" with "invalidData".
#         And the user fills all fields of "Vehicle" with "invalidData".

#     Scenario: Validate all fields of create customer/vehicle with exceeding Charater Limits.
#     When the user fills all fields of "Customer" with "exceedingCharaterLimits".
#         And the user fills all fields of "Vehicle" with "exceedingCharaterLimits".

#     Scenario: Validate all fields of create customer/vehicle with already existing vehicle plate.
#     When the user fills all fields of "Customer" with "validData".
#         And the user fills all fields of "Vehicle" with "validData".
#         And the user hits the "Save" button from the "footerButtons".
#     Then the "Vehicle already registered" should appear.
#         And the user hits the "okBtn" button.

#     Scenario: Validate all fields of create customer/vehicle with already existing customer email.
#     When the user enters valid value in "vehiclePlate" at "Vehicle" screen.
#         And the user hits the "Save" button from the "footerButtons".
#     Then the "Customer already exist with this email" should appear.
#          And the user hits the "okBtn" button.

# # Validations with Phone is remaining.
#     Scenario: Validate all fields of create customer/vehicle with new data.
#     When the user fills all fields of "Customer" with "newData".
#         And the user fills all fields of "Vehicle" with "newData".
#         And the user hits the "Save" button from the "footerButtons".
#     Then the "Information added successfully" should appear.
#          And the user hits the "okBtn" button.

#     Scenario: Create a vehicle of an existing customer.
#     # Given the user is on New Inspections screen.
#     When the user selects "Mobile Number" value from the "searchByDropdown" dropdown.
#         And the user selects customer.
#         And the user hits the "Add New Vehicle" button from the "addNewVehicleBtn".
#     Then All data of the "Customer" should be correct.
#     When the user fills all fields of "Vehicle" with "validData".
#         And the user hits the "Save" button from the "footerButtons".
#     Then the "Vehicle already registered" should appear.
#         And the user hits the "okBtn" button.
#         And the user enters valid value in "vehiclePlate" at "Vehicle" screen.
#         And the user hits the "Save" button from the "footerButtons".
#     Then the "Vehicle added successfully" should appear.
#     When the user hits the "okBtn" button.
#     Then the "{string}" field should be visible.