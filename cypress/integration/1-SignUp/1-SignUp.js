/// <reference types="Cypress" />

import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

const SignUpLocators = require("../../Locators/SignUpLocators.json")
const commonLocators = require("../../Locators/commonLocators.json")

Given("The user is on the Sign Up page.", () => {
    // cy.visit("https://develop.torque360.co/signup")
    // Click Get Started button to open form
    cy.get(SignUpLocators.signUpLogo).should("be.visible")
})

Then("the user enters Name: {string}", (nameField) => {
    // Fill all fields
    cy.get(SignUpLocators.nameField).clear()  
    cy.get(SignUpLocators.nameField).type(nameField)  
})

And("the user enters {string} in Email field", (emailStatus) => {
    // Fill EMAIL field
    cy.fixture('example').then(data => {
        cy.get(SignUpLocators.emailField).clear()  
        cy.get(SignUpLocators.emailField).type(data.signUpData[emailStatus])
    })
})

And("the user enters phone: {string}", (phoneField) => {
    // Fill all fields
    cy.get(SignUpLocators.phoneField).clear()  
    cy.get(SignUpLocators.phoneField).type(phoneField)
})

And("the user enters {string} in Password field w.r.t the password policies.", (passwordStatus) => {
    // Fill password field
    cy.fixture('example').then(data => {
        cy.get(SignUpLocators.passwordField).clear()  
        cy.get(SignUpLocators.passwordField).type(data.signUpData[passwordStatus])
    })
})

And("the user enters {string} in Password field", (password) => {
    // Invalid password field assertions
    cy.get(SignUpLocators.passwordField).type(password)
})

And("the color of the text: {string} should be {string}", (errorMessage, textColor) => {
    cy.get(SignUpLocators.passwordError).contains(errorMessage).should("have.css", "color", textColor)
})

And("the user clicks Submit button", () => {
    // Click Submit button
    cy.get(SignUpLocators.submitButton).click()
})

When("the user enters no value in {string} field.", (field) => {
    cy.get(SignUpLocators[field]).clear()
})

When("the user enters data with exceeding characters in all fields, then {string} error should appear.", (errorMessage) => {
    cy.fixture("example").then(exampleData => {
        let data = exampleData.signUpData.exceedingCharactersLimitData
        for(let loc in data) {
            cy.get(SignUpLocators[loc]).clear()
            cy.get(SignUpLocators[loc]).type(data[loc])
            
            cy.get(SignUpLocators[loc]).parent().parent().find(commonLocators.validationError).invoke("text").then(errorText => {
                expect(errorText).to.equals(errorMessage)
                cy.get(SignUpLocators[loc]).type("{backspace}")
                cy.get(SignUpLocators[loc]).parent().parent().find(commonLocators.validationError).should("not.exist")
            })
        }
    })
})

Then("the {string} should appear.", (toastMessage) => {
    // Reading fixture file.
    cy.fixture('example').then(data => {
        if (toastMessage === "SuccessMessage"){
            cy.get(commonLocators.toastMessage).should("have.text", data.signUpData.SuccessMessage1 + data.signUpData.validEmail + data.signUpData.SuccessMessage2)

            cy.readFile("cypress/fixtures/example.json", (err, data) => {
                if (err) {
                    return console.error(err);
                };
            }).then((data) => {
                // get Unique Email.
                cy.getUniqueEmail(data.signUpData.validEmail).then(newEmail => {
                    data.signUpData.validEmail = newEmail
                    data.credentials.Username = newEmail
                    cy.writeFile("cypress/fixtures/example.json", JSON.stringify(data))
                })
            })
            // cy.writeFile('/cypress/fixtures/example.json', { "validEmail": "qa.abdullah360+9@gmail.com" })
        } else {
                cy.get(commonLocators.toastMessage).should("have.text", data.signUpData[toastMessage])
                // cy.get(commonLocators.toastMessage).invoke("text").then(observeMessage => {
                //     cy.log(observeMessage)
                //     expect(observeMessage).contains(data.signUpData[toastMessage])
        }

        cy.get(SignUpLocators.okBtn).click()
    })
})

Given("the user navigates to the registration URL received at user email address.", () => {
    cy.fixture('example').then(data => {
        cy.gotoRegLinkFromEmail(Cypress.env("MAILOSAUR_Server_ID"), data.signUpData.validEmail)
    })
})