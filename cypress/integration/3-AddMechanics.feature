Feature: First Login Add Mechanics.

    Description: This feature test all test cases of Adding Mechanics process.

    Scenario: The user is on Add Mechanics screen. 
    Given the user navigates to Add Mechanics screen.
    Then the "closeButton" field should be visible.
        And the "name" field should be visible.
        And the "emailField" field should be visible.
        And the "sendInvite" field should be visible.

    # Scenario: Positive flow
    # When Positive flow

    Scenario: Fill form with all blank fields
    When the user hits the "sendInvite" button.
    Then the "your name is required" validation error should appear.
        And the "your email is required" validation error should appear.

    Scenario: Fill form with blank name field.
    When the user enters valid value in "emailField".
        And the user hits the "sendInvite" button.
    Then the "your name is required" validation error should appear.
    # When the user enters space in "name".
    #     And the user hits the "sendInvite" button.
    # Then the "your name is required" validation error shoulad appear.

# Validation not applied.

    # Scenario: Fill form with invalid name field.
    # When the user enters invalid value in "name".
    #     And the user enters "validEmail" in Email field
    #     And the user hits the "sendInvite" button.
    # Then the "Special Character Not Allowed" validation error should appear.

    Scenario: Fill form with blank email field.
    When the user enters valid value in "name".
        And the user enters no value in "emailField".
        And the user hits the "sendInvite" button.
    Then the "your email is required" validation error should appear.
    When the user enters space in "emailField".
        And the user hits the "sendInvite" button.
    Then the "the email that you have entered is incorrect" validation error should appear.

    Scenario: Fill form with invalid email field.
    When the user enters valid value in "name".
        And the user enters invalid value in "emailField".
        And the user hits the "sendInvite" button.
    Then the "the email that you have entered is incorrect" validation error should appear.

    Scenario: Fill form with already existing email fields.
    When the user enters valid value in "name".
        And the user enters "AlreadyExistingEmail" value in "emailField".
        And the user hits the "sendInvite" button.
    Then the "AlreadyExistingEmailError" should appear.

    Scenario: Fill form with all valid fields.
    Given the user navigates to Add Mechanics screen.
    When the user enters valid value in "name".
        And the user enters valid value in "emailField".
        And the user hits the "sendInvite" button.
    Then the "SuccessMessage" should appear.

    Scenario: Fill form but close the lead form popup.
    Given the user navigates to Add Mechanics screen.
    When the user enters valid value in "name".
        And the user enters valid value in "emailField".
        And the user clicks Close button
    Then the "addMechanicsConfirmText" with text "addMechanicsConfirmText" should appear.

# Scenarios of Setup mechanic
    Scenario: Get registration URL from email and naviagete there.
    Given the user navigates to the registration URL received at employee email address. 
    Then the "ActivationSuccessMessage" should appear.
    When the user hits the "setupAccountBtn" button.
    Then the "name" field should be visible.
        And the "emailField" field should be visible.
        And the "phoneField" field should be visible.
        And the "passwordField" field should be visible.
        And the "confirmPasswordField" field should be visible.
        And the "createAccountbtn" field should be visible.

    # <space> in name missing
    
    Scenario: Setup the mechanic with all blank fields
    When the user hits the "createAccountbtn" button.
    Then the "Phone Number Required" validation error should appear.
        Then the "Password Required" validation error should appear.

    Scenario: Setup the mechanic with blank name field.
    When the user enters no value in "name".
        And the user enters valid value in "phoneField".
        And the user enters valid value in "passwordField".
        And the user enters valid value in "confirmPasswordField".
        And the user hits the "createAccountbtn" button.
    Then the "Name Required" validation error should appear.

    # Scenario: Setup the mechanic with blank email field.
    # When the user enters valid Name: "SQAE"
    #     And the user enters phone: "5452345323"
    #     And the user enters "validPassword" in Password field w.r.t the password policies.
    #     And the user clicks Submit button
    # Then the "Email Required" validation error should appear.

    # Scenario: Setup the mechanic with invalid email field.
    # When the user enters valid Name: "SQAE"
    #     And the user enters "InValidEmail" in Email field
    #     And the user enters phone: "5452345323"
    #     And the user enters "validPassword" in Password field w.r.t the password policies.
    #     And the user clicks Submit button
    # Then the "Invalid Email" validation error should appear.

    # Scenario: Setup the mechanic with already existing email fields.
    # When the user enters valid Name: "SQAE"
    #     And the user enters "AlreadyExistingEmail" in Email field
    #     And the user enters phone: "5452345323"
    #     And the user enters "validPassword" in Password field w.r.t the password policies.
    #     And the user clicks Submit button
    # Then the "AlreadyExistingEmailError" should appear.

    Scenario: Setup the mechanic with blank phone field.
    When the user enters valid value in "name".
        And the user enters no value in "phoneField".
        And the user enters valid value in "passwordField".
        And the user enters valid value in "confirmPasswordField".
        And the user hits the "createAccountbtn" button.
    Then the "Phone Number Required" validation error should appear.

    Scenario: Setup the mechanic with invalid phone field.
    When the user enters valid value in "name".
        And the user enters invalid value in "phoneField".
        And the user enters valid value in "passwordField".
        And the user enters valid value in "confirmPasswordField".
        And the user hits the "createAccountbtn" button.
    Then the "Invalid Phone Number" validation error should appear.

    Scenario: Setup the mechanic with invaild passwords.
    When the user enters valid value in "name".
        And the user enters no value in "phoneField".
        And the user enters no value in "passwordField".
        And the user enters no value in "confirmPasswordField".
        And the user hits the "createAccountbtn" button.
    Then the "Password Required" validation error should appear.
        And the color of the text: "At least contains 1 capital letter" should be "rgba(174, 182, 183, 0.796)"
        And the color of the text: "At least contains 1 Symbol" should be "rgba(174, 182, 183, 0.796)"
        And the color of the text: "At least contains 1 digit" should be "rgba(174, 182, 183, 0.796)"
        And the color of the text: "Minimum 8 Characters" should be "rgba(174, 182, 183, 0.796)"
    When the user enters "T" in Password field
        And the user clicks Submit button
    Then the "Password Policy:" validation error should appear.
        And the color of the text: "At least contains 1 capital letter" should be "rgb(33, 196, 108)"
    When the user enters "orq@" in Password field
        And the user clicks Submit button
    Then the "Password Policy:" validation error should appear.
        And the color of the text: "At least contains 1 Symbol" should be "rgb(33, 196, 108)"
    When the user enters "3" in Password field
        And the user clicks Submit button
    Then the "Password Policy:" validation error should appear.
        And the color of the text: "At least contains 1 digit" should be "rgb(33, 196, 108)"
    When the user enters "60" in Password field
        And the user clicks Submit button
    Then the color of the text: "Minimum 8 Characters" should be "rgb(33, 196, 108)"

    Scenario: Setup the mechanic with mismatch passwords.
    When the user enters valid value in "name".
        And the user enters valid value in "phoneField".
        And the user enters valid value in "passwordField".
        And the user enters invalid value in "confirmPasswordField".
        And the user hits the "createAccountbtn" button.
    Then the "Passwords do not match!" validation error should appear.

    Scenario: Setup the mechanic with all valid fields.
    When the user enters valid value in "name".
        And the user enters valid value in "phoneField".
        And the user enters valid value in "passwordField".
        And the user enters valid value in "confirmPasswordField".
        And the user hits the "createAccountbtn" button.
    Then the "SuccessMessage" should appear.
    
    # dfgsdgfssdfsdf

    Scenario: The user should not Setup the mechanic with more than 320 characters in the email. 
    When the user enters valid User full name.
        And the user enters email which is more than 320 characers long.
        And the user enters valid phone number
        And the user enters valid password w.r.t the password policies. 
        And the user clicks Sign Up button.
    Then the Characters limit exceeded error message should appear under the Email field.

    Scenario: The user should not Setup the mechanic with more than 40 characters in the Name. 
    When the user enters Name which is more than 40 characers long.
        And the user enters valid email.
        And the user enters valid phone number
        And the user enters valid password w.r.t the password policies. 
        And the user clicks Sign Up button.
    Then the Characters limit exceeded error message should appear under the Name field.

    Scenario: The user should not Setup the mechanic with more than 15 characters in the Phone. 
    When the user enters valid User full name.
        And the user enters valid email.
        And the user enters phone number having more than 15 characters.
        And the user enters valid password w.r.t the password policies. 
        And the user clicks Sign Up button.
    Then the Characters limit exceeded error message should appear under the Phone field.

    Scenario: The user should not Setup the mechanic with more than 40 characters in the Password. 
    When the user enters vaild Name.
        And the user enters valid email.
        And the user enters valid phone number
        And the user enters password with more than 40 characers.
        And the user clicks Sign Up button.
    Then the Characters limit exceeded error message should appear under the Password field.