Feature: Customer And Vehicle test cases

    This Feature: contains all the test cases of Customer and Vehicle CRUD options

    Scenario: All fields of the Customer and vehicle should be visible.
    Given the user is on New Inspections screen.
        And the user hits the "Add Customer" button from the "addCustomerBtn".
        And the user hits the "Add Vehicle" button from the "footerButtons".
    Then all fields of the Customer and Vehicle screen should be visible.

    Scenario: Validate all fields of create customer/vehicle Without filling any field.
    When the user hits the "Save" button from the "footerButtons".
    Then the "name" field at "Customer" section, should be underlined as red.
        And the "customerEmail" field at "Customer" section, should be underlined as red.
        And the "phone" number field at "Customer" section, should be underlined as red.
        And the "vehicleModelYear" field at "Vehicle" section, should be underlined as red.
        And the "vehicleModel" field at "Vehicle" section, should be underlined as red.
        And the "vehicleMake" field at "Vehicle" section, should be underlined as red.
        And the "vehiclePlate" field at "Vehicle" section, should be underlined as red.
        And the "vehicleFuelTypeDropdown" field at "Vehicle" section, should be underlined as red.
        And the "vehicleColor" field at "Vehicle" section, should be underlined as red.

    Scenario: Validate all valid fields of create customer/vehicle with invalid Data.
    When the user fills all fields of "Customer" with "invalidData".
        And the user fills all fields of "Vehicle" with "invalidData".

    Scenario: Validate all fields of create customer/vehicle with exceeding Charater Limits.
    When the user fills all fields of "Customer" with "exceedingCharaterLimits".
        And the user fills all fields of "Vehicle" with "exceedingCharaterLimits".

    Scenario: Validate all fields of create customer/vehicle with already existing vehicle plate.
    When the user fills all fields of "Customer" with "validData".
        And the user fills all fields of "Vehicle" with "validData".
        And the user hits the "Save" button from the "footerButtons".
    Then the "Vehicle already registered" should appear.
        And the user hits the "okBtn" button.

    Scenario: Validate all fields of create customer/vehicle with already existing customer email.
    When the user enters valid value in "vehiclePlate" at "Vehicle" screen.
        And the user hits the "Save" button from the "footerButtons".
    Then the "Customer already exist with this email" should appear.
         And the user hits the "okBtn" button.

    Scenario: Validate all fields of create customer/vehicle with new data.
    When the user fills all fields of "Customer" with "newData".
        And the user fills all fields of "Vehicle" with "newData".
        And the user hits the "Save" button from the "footerButtons".
    Then the "Information added successfully" should appear.
         And the user hits the "okBtn" button.

    Scenario: Create a vehicle of an existing customer.
    When the user again goes to New Inspections screen.
    Given the user is on New Inspections screen.
    When the user selects "Mobile Number" value from the "searchByDropdown" dropdown.
        And the user selects customer.
        And the user hits the "Add New Vehicle" button from the "addNewVehicleBtn".
    Then All data of the "Customer" should be correct.
    When the user fills all fields of "Vehicle" with "validData".
        And the user hits the "Save" button from the "footerButtons".
    Then the "Vehicle already registered" should appear.
        And the user hits the "okBtn" button.
        And the user fills all fields of "Vehicle" with "addNewVehicleData".
        And the user hits the "Save" button from the "footerButtons".
    Then the "Vehicle added successfully" should appear.
    When the user hits the "okBtn" button.
    Then the "addCustomerBtn" field should be visible.