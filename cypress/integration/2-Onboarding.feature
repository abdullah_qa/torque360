Feature: First Login Onboarding.

    Description: This feature test all test cases of first time login in the app and onboarding process.

    Scenario: The user is on Onboarding screen. 
    Given the user navigates to onboarding screen.
    Then the "profileImageHolder" field should be visible.
        And the "addressField" field should be visible.
        And the "mobileField" field should be visible.
        And the "businessNameField" field should be visible.
        And the "emailField" field should be visible.
        And the "timezoneDropdown" field should be visible.
        And the "ownernameField" field should be visible.
        And the "phoneField" field should be visible.
        And the "currencyDropdown" field should be visible.

    # Scenario: Positive flow
    # When Positive flow

    # Scenario: Clear all fields at step 1
    # When the user enters no value in "addressField".
    #     And the user enters valid value in "mobileField".
    #     And the user enters valid value in "addressField".
    #     And the user enters valid value in "businessNameField".
    #     And the user enters valid value in "emailField".
    #     And the user enters valid value in "timezoneDropdown".
    #     And the user enters valid value in "ownernameField".
    #     And the user enters valid value in "phoneField".

    Scenario: Fill the onboarding process step 1 with all blank fields' values and wrong formatted profile image.
    When the user upload "wrong_format.json" for profile.
        And the user hits the "Next" button.
    Then the "addressField" field should be underlined as red.
        And the "mobileField" field should be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.
        And the "File Format Not Supported" error appears under profile image.

    Scenario: Fill the onboarding process step 1 with all blank fields' values and invalid profile image.
    When the user upload "invalid_image.png" for profile.
        And the user hits the "Next" button.
    Then the "addressField" field should be underlined as red.
        And the "mobileField" field should be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.
        And the "Image File exceeds size limit (800kb)" error appears under profile image.

    Scenario: Fill the onboarding process step 1 with all blank fields' values and valid profile image.
    When the user upload "valid_image.png" for profile.
        And the user hits the "Next" button.
    Then the "addressField" field should be underlined as red.
        And the "mobileField" field should be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: Fill the onboarding process step 1 with blank fields' values.
    When the user hits the "Next" button.
    Then the "addressField" field should be underlined as red.
        And the "mobileField" field should be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.
    
    # Scenario: Fill the onboarding process step 1 with <spaces> in fields' values.
    # When the user enters space in "addressField".
    #     And the user enters space in "mobileField".
    #     And the user enters space in "businessNameField".
    #     And the user enters space in "emailField".
    #     And the user enters space in "ownernameField".
    #     And the user enters space in "phoneField".
    #     And the user hits the "Next" button.
    # Then the "addressField" field should be underlined as red.
    #     And the "mobileField" field should be underlined as red.
    #     And the "businessNameField" field should be underlined as red.
    #     And the "emailField" field should be underlined as red.
    #     And the "timezoneDropdown" field should be underlined as red.
    #     And the "ownernameField" field should be underlined as red.
    #     And the "phoneField" field should be underlined as red.
    #     And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address.
    When the user enters valid value in "addressField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, enter invalid Mobile.
    When the user enters invalid value in "mobileField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile.
    When the user enters valid value in "mobileField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile | Business name.
    When the user enters valid value in "businessNameField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, enter invalid Business Email.
    When the user enters invalid value in "emailField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile | Business name | Business Email.
    When the user enters valid value in "emailField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should NOT be underlined as red.
        And the "timezoneDropdown" field should be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile | Business name | Business Email | Time zone.
    When the user selects valid value from the "timezoneDropdown".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should NOT be underlined as red.
        And the "timezoneDropdown" field should NOT be underlined as red.
        And the "ownernameField" field should be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile | Business name | Business Email | Time zone | Owner name.
    When the user enters valid value in "ownernameField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should NOT be underlined as red.
        And the "timezoneDropdown" field should NOT be underlined as red.
        And the "ownernameField" field should NOT be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile | Business name | Business Email | Time zone | Owner name and INVALID Phone.
    When the user enters invalid value in "phoneField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should NOT be underlined as red.
        And the "timezoneDropdown" field should NOT be underlined as red.
        And the "ownernameField" field should NOT be underlined as red.
        And the "phoneField" field should be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: At step 1, leave all field(s) empty other than Business Address | Mobile | Business name | Business Email | Time zone | Owner name | Phone.
    When the user enters valid value in "phoneField".
        And the user hits the "Next" button.
    Then the "addressField" field should NOT be underlined as red.
        And the "mobileField" field should NOT be underlined as red.
        And the "businessNameField" field should NOT be underlined as red.
        And the "emailField" field should NOT be underlined as red.
        And the "timezoneDropdown" field should NOT be underlined as red.
        And the "ownernameField" field should NOT be underlined as red.
        And the "phoneField" field should NOT be underlined as red.
        And the "currencyDropdown" field should be underlined as red.

    Scenario: Move to step 2 will all valid values.
    When the user selects valid value from the "currencyDropdown".
        And the user hits the "Next" button.
    Then the "noOfMechanicsField" field should be visible.
        And the "noOfBaysField" field should be visible.
        And the "inputSlider" field should be visible.
        And the "radioGroups" field should be visible.
        And the "inspectionFeeField" field should be visible.
        And the "setTaxField" field should be visible.

    # Scenario: At step 2, hit Next button without entering any field(s)
    # When the user hits the "Next" button.
    # Then the "mechanicsLabel" label should be highlighted as Red.
    #     And the "baysLabel" label should be highlighted as Red.
    # When the user enters space in "noOfMechanicsField".
    #     And the user enters space in "noOfBaysField".
    #     And the user hits the "Next" button.
    # Then the "mechanicsLabel" label should be highlighted as Red.
    #     And the "baysLabel" label should be highlighted as Red.

    Scenario: Move to step 3 with all valid values.
    When the user add total "mechanics" out of "15" through the slider.
        And the user add total "Beys" out of "8" through the slider.
        And the user chooses "Yes" for "ChargeForInspection" options.
        And the user enters valid value in "inspectionFeeField".
        And the user chooses "Fixed Amount" for "chargeTax" options.
        And the user enters valid value in "setTaxField".
        And the user hits the "Next" button.
    Then the all features and services should be visible.

    Scenario: Move to step 4 with all valid values:
    When the user selects features from the available.
        And the user enters valid value in "customServicesField".
        And the user hits the "Next" button. 
    Then the "vendorsField" field should be visible.
        And the "avgMonRevenueField" field should be visible.
        And the "inputSlider" field should be visible.
        And the "radioGroups" field should be visible.
        And the "softwareUsedBeforeField" field should be visible. 

    Scenario: Onboarding confirmation wizard should open upon submitting step 4.
    When the user enters valid value in "vendorsField".
        And the user add total "Monthly Revenue" out of "100000" through the slider.
        And the user chooses "Some Software" for "oldManagement" options.
        And the user enters valid value in "softwareUsedBeforeField".
        And the user hits the "Finish" button. 
    Then the "submitConfirmWizardButton" field should be visible.
    # Then the "SuccessMessage" should appear.
