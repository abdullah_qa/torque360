Feature: Estimate

    This Feature: contains all the test cases of Estimate CRUD options

    # Scenario: positive flow
    # When positive flow
    
    Scenario: User should not be allowed to Generate Estimate without entering any field.
    Given the user is on New Estimate screen.
    Then the "generateEstimateBtn" field should not be Clickable.
        And the "saveDraftBtn" field should not be Clickable.
        And the "otherJobType" field should not be Clickable.
    #     And the "Save Draft" button from "generateBtns" should not be Clickable.
        
    Scenario: User should NOT be allowed to Generate DVI by adding customer/vehicle but without general/detailed inspection.
    When the user selects "Mobile Number" value from the "searchByDropdown" dropdown.
    When the user selects customer and Vehicle, the data appear should be correct.
    # When the user hits the "generateEstimateBtn" button.
    # Then the "Select atleast one inspection type." error should appear.

    # Todo: Customer and vehicle preview validations. 

    # Scenario: Add Parts/Services/Labor/Canned Services. 
    # When the user hits the "Add Parts" button from the "addPartsBtns".
    #     And the user add part using "searchPart" field.
    #     And the user add labor using "laborSearch" field.
    #     And the user add supply using "supplySearch" field.
    #     # And the user add canned service using "searchPart" field.
    # Then 

    Scenario: Gnerate Estimate 
    When the user selects "jobTypes".
        And the user enters valid value in "otherJobType".
        And the user hits the "Problem Type" button from the "problemTypeBtn".
        And the user enters valid value in "problemSearch".
        And the user selects all the problems appears in dropdown.
        And the user unselects few selected problems appears in dropdown.
        And the user hits the "Add Parts" button from the "addPartsBtns".
        And the user enters valid value in " ".
        And the user enters valid value in "notes".
        And the user selects estimated "turnaroundTime".
        And the user enters valid value in "manHours".
        And the user enters valid value in "tax".
        And the user enters valid value in "discount".
        And the user hits the "generateEstimateBtn" button.
    Then the "Estimate has been created" success message should appear.
