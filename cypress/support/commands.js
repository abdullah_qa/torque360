/// <reference types="Cypress" />

import 'cypress-mailosaur'

const SignInLocators = require("../Locators/SignIn.json")
const commonLocators = require("../Locators/commonLocators.json")

let LOCAL_STORAGE_MEMORY = {};

Cypress.Commands.add("gotoRegLinkFromEmail", (SERVER_ID, sentToEmail) => {
    cy.log("Getting registration link from email. " + sentToEmail)
    cy.mailosaurGetMessage(SERVER_ID, {sentFrom: "torquepos@gmail.com"}).then(email => {
        const firstLink = email.html.links[0]
        cy.log(firstLink.text) 
        // var url = firstLink.href
        var url = firstLink.href.replace("portal", "www.qaportal")
        cy.mailosaurDeleteAllMessages(SERVER_ID)

        cy.visit(url)
    })
})

Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});

Cypress.Commands.add('loginWithApi2', (username, password) => {
    cy.session([username, password], () => {
        cy.request({
            method: "POST",
            url: "https://api.torque360.co/api/auth/login",
            headers: {
                "Host": "api.torque360.co",
                "Connection": "keep-alive",
                "Accept": "application/json, text/plain, */*",
                // "Authorization": "Bearer undefined",
                "Origin": "https://www.develop.portal.torque360.co",
                "Referer": "https://www.develop.portal.torque360.co/",
            },
            followRedirect: true,
            form: false,
            body: { 
                "email": username, 
                "password": password, 
                "showPassword": "", 
                "type": 0 },
        }).then((response) => {
            expect(response.status).equal(201)
            // Storing user Data in Cache
            cy.window().then((window) => {
                window.localStorage.setItem("userData", JSON.stringify(response.body));
                // cy.writeFile("cypress/fixtures/userData.json", JSON.stringify(response.body))
                cy.log("The user logged in successfully")
            })
        })
    })
},
{
    validate() {
        cy.request('/dashboard').its('statusCode').should('eq', 200)
},
})

Cypress.Commands.add("loginWithApi", (username, password) => {

    cy.request({
        method: "POST",
        url: "https://api.torque360.co/api/auth/login",
        headers: {
            "Host": "api.torque360.co",
            "Connection": "keep-alive",
            "Accept": "application/json, text/plain, */*",
            // "Authorization": "Bearer undefined",
            "Origin": "https://www.develop.portal.torque360.co",
            "Referer": "https://www.develop.portal.torque360.co/",
        },
        followRedirect: true,
        form: false,
        body: { 
            "email": username, 
            "password": password, 
            "showPassword": "", 
            "type": 0 },
    }).then((response) => {
        expect(response.status).equal(201)
        // Storing user Data in Cache
        cy.window().then((window) => {
            window.localStorage.setItem("userData", JSON.stringify(response.body));
            // cy.writeFile("cypress/fixtures/userData.json", JSON.stringify(response.body))
            cy.log("The user logged in successfully")
        })
    })
})

Cypress.Commands.add("getUserDetailToken", (authToken) => {
    cy.intercept("GET", "/dashboard").as("dashboard")

    cy.request({
        method: "GET",
        url: "https://api.torque360.co/api/dashboard/getuserdetails",
        headers: {
            "Host": "api.torque360.co",
            "Connection": "keep-alive",
            "Accept": "application/json, text/plain, */*",
            "Authorization": "Bearer " + authToken,
            "Origin": "https://www.develop.portal.torque360.co",
            "Referer": "https://www.develop.portal.torque360.co/",
        },
        followRedirect: true,
    }).then((response) => {
        expect(response.status).equal(200)
        cy.log(response.body.message)
    })
})

Cypress.Commands.add("loginWithUI", (username, password) => {
    cy.visit("/")
    // Check if the user is on the login page. 
    cy.get(SignInLocators.emailField).should("be.visible")

    // Enter credentials and login.
    cy.get(SignInLocators.emailField).type(username)
    cy.get(SignInLocators.passwordField).type(password)

    cy.get(SignInLocators.submitButton).click()
})

Cypress.Commands.add("getUniqueEmail", (previousEmail) => {
    let halfEmail = previousEmail.split("+")[1]
    let uniqueNumber = halfEmail.split("@")[0]
    uniqueNumber = parseInt(uniqueNumber) + 1
    let newEmail = "qa.abdullah360+" + uniqueNumber + "@gmail.com"
    return newEmail
})

Cypress.Commands.add("getUniqueName", (previousNum) => {
    let uniqueNumber = previousNum + 1
    cy.numToWords(uniqueNumber).then(newNumber => {
        return "qa_" + newNumber
    })
})

Cypress.Commands.add("getUniqueVehiclePlate", (previousVehiclePlate) => {
    let VehiclePlate = previousVehiclePlate.split("-")[0]
    let uniqueNumber = previousVehiclePlate.split("-")[1]
    uniqueNumber = parseInt(uniqueNumber) + 2
    let newVehiclePlate = VehiclePlate + "-" + uniqueNumber
    return newVehiclePlate
})

Cypress.Commands.add("getUniqueEmpEmail", (previousEmail) => {
    let halfEmail = previousEmail.split("+emp")[1]
    let uniqueNumber = halfEmail.split("@")[0]
    uniqueNumber = parseInt(uniqueNumber) + 1
    let newEmail = "qa.abdullah360+emp" + uniqueNumber + "@gmail.com"
    return newEmail
})

Cypress.Commands.add("getAfterValue", (selector, pseudo, property) => {
    cy.get(selector)
        .parent().then($els => {
            // get Window reference from element
            const win = $els[0].ownerDocument.defaultView
            // use getComputedStyle to read the pseudo selector
            const after = win.getComputedStyle($els[0], pseudo)
            // read the value of the `content` CSS property
            const contentValue = after.getPropertyValue(property)
            // the returned value will have double quotes around it, but this is correct
            return contentValue
            // expect(contentValue).to.eq("rgb(229, 57, 53)")
        })
})

var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

Cypress.Commands.add("numToWords", (num) => {
    if ((num = num.toString()).length > 9) return 'overflow'
    let n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/)
    if (!n) return; var str = ''
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : ''
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : ''
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : ''
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : ''
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]])  : ''
    return str
})
